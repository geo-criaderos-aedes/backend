FROM debian:latest

RUN apt-get update && apt-get install -y apache2 \
    libapache2-mod-wsgi \
    build-essential \
    python \
    python-dev\
    python-pip \
    unzip \
 && apt-get clean \
 && apt-get autoremove \
 && rm -rf /var/lib/apt/lists/*

# Copy over and install the requirements
COPY requirements.txt /var/www/apache-flask/proyectoservidor/requirements.txt
RUN pip install -r /var/www/apache-flask/proyectoservidor/requirements.txt

# Copy over the apache configuration file and enable the site
COPY config/Apache/FlaskApp.conf /etc/apache2/sites-available/apache-flask.conf
RUN a2ensite apache-flask
RUN a2enmod headers

# Copy over the wsgi file
COPY config/Apache/flaskapp.wsgi /var/www/apache-flask/apache-flask.wsgi

COPY config.py create_db.py database.py run.py utils.py /var/www/apache-flask/proyectoservidor/
COPY app /var/www/apache-flask/proyectoservidor/app

COPY config/OpenLayers/OpenLayers-2.13.1.zip /tmp
RUN unzip /tmp/OpenLayers-2.13.1.zip -d /var/www/apache-flask/proyectoservidor/app/static/js

RUN a2dissite 000-default.conf
RUN a2ensite apache-flask.conf

EXPOSE 80

WORKDIR /var/www/apache-flask/proyectoservidor

# CMD ["/bin/bash"]
CMD  /usr/sbin/apache2ctl -D FOREGROUND
# The commands below get apache running but there are issues accessing it online
# The port is only available if you go to another port first
# ENTRYPOINT ["/sbin/init"]
# CMD ["/usr/sbin/apache2ctl"]
