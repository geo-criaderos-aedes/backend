## Instalación Flask

* https://pypi.org/project/Flask/0.11/
* https://doc.ebichu.cc/flask/

```
docker build -t servidor .
docker run -d -p8080:80 servidor
```

## Instalación PostgreSQL

* https://help.ubuntu.com/community/PostgreSQL

```
sudo apt-get install postgresql postgresql-contrib pgadmin3
```

## Instalador GeoServer

* http://geoserver.org/release/2.7.6/
