# Import flask and template operators
from flask import Flask
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for

from functools import wraps

# Define the WSGI application object
app = Flask(__name__)

# Configurations
app.config.from_object('config')
app.secret_key = app.config['SECRET_KEY']

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        user_id = session.get('id_usuario')
        if not user_id:
            return redirect(url_for('auth.login'))
        return f(*args, **kwargs)
    return decorated_function

# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    id_usuario = session['id_usuario']
    return render_template('404.html', id_usuario=id_usuario), 404

@app.route('/no_autorizado', methods=['GET', 'POST'])
def no_autorizado():
    id_usuario = session['id_usuario']
    return render_template('error_permisos.html', id_usuario=id_usuario)

# Import a module / component using its blueprint handler variable
from app.mod_auth.views import mod_auth as auth_module
from app.mod_user.views import mod_user as user_module
from app.mod_roles.views import mod_roles as roles_module
from app.mod_movil.views import mod_movil as movil_module
from app.mod_tbls1.views import mod_tbls1 as tbls1_module
from app.mod_mapa.views import mod_mapa as mapa_module
from app.mod_reporte.views import mod_reporte as reporte_module

# Register blueprint(s)
app.register_blueprint(auth_module)
app.register_blueprint(user_module)
app.register_blueprint(roles_module)
app.register_blueprint(movil_module)
app.register_blueprint(tbls1_module)
app.register_blueprint(mapa_module)
app.register_blueprint(reporte_module)

from database import db_session

@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()
