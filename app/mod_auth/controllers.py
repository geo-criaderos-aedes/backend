from database import db_session

# Import module models
from app.mod_user.models import Usuario

from sqlalchemy import exc

from werkzeug.security import generate_password_hash, \
check_password_hash

def set_password(password):
    return generate_password_hash(password)

def check_password(pw_hash, password):
    return check_password_hash(pw_hash, password)

def verificar_login(username, password):
    try:
        contador = db_session.query(Usuario). \
        filter_by(id_usuario=username, estado='ACTIVO').count()

        if contador == 0:
            return False

        usuario = db_session.query(Usuario).\
        filter_by(id_usuario=username).one()

        usuario_contrasena = usuario.contrasena

        return check_password(usuario_contrasena, password)
    except exc.SQLAlchemyError, e:
        return False

def roles_required(id_usuario, roles):
    """Decorador que especifica que un usuario debe tener todos los 
    roles especificados."""
    usuario = db_session.query(Usuario).filter_by(id_usuario=\
    id_usuario).one()
    lista_roles = usuario.roles

    lista_permisos = []

    for rol in lista_roles:
        for permiso in rol.permisos:
            lista_permisos.append(permiso.nombre)

    continuar = False

    for rol in roles:
        continuar = False
        for permiso in lista_permisos:
            if rol == permiso:
                continuar = True
                break

        if not continuar:
            return False

    return True

def roles_accepted(id_usuario, roles):
    """Decorador que especifica que un usuario debe tener al menos uno
    de los roles especificados"""
    usuario = db_session.query(Usuario).filter_by(id_usuario=\
    id_usuario).one()
    lista_roles = usuario.roles

    lista_permisos = []

    for rol in lista_roles:
        for permiso in rol.permisos:
            lista_permisos.append(permiso.nombre)

    for rol in roles:
        for permiso in lista_permisos:
            if rol == permiso:
                return True

    return False
