# Import flask dependencies
from flask import Blueprint
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from app import login_required
from app.mod_auth import controllers

# Define the blueprint
mod_auth = Blueprint('auth', __name__, url_prefix='/auth')

# Set the route and accepted methods
@mod_auth.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        validacion = controllers.verificar_login(username, password)
        if validacion:
            session['id_usuario'] = username
            return redirect(url_for('auth.menu_principal'))
        else:
            error = 'Login Incorrecto'
    return render_template('auth/login.html', error=error, password='')

@mod_auth.route('/logout')
def logout():
    session.pop('id_usuario', None)
    session.clear()
    return redirect(url_for('auth.login'))

@mod_auth.route('/menu_principal')
@login_required
def menu_principal():
    error = None
    id_usuario = session['id_usuario']
    session['busqueda'] = False
    return render_template('auth/menu_principal.html', error=error, \
    id_usuario=id_usuario)
