from database import db_session

# Import module models
from app.mod_tbls1.models import RegistroS1, CriaderoA, CriaderoB, CriaderoC

from sqlalchemy import exc

from sqlalchemy.sql import text

import datetime

def load_puntos_visitados(desde, hasta):
    delete_str = 'DELETE FROM puntos_visitados'

    try:
        db_session.execute(text(delete_str), {})
        db_session.flush()

        registros = db_session.query(RegistroS1).\
        filter(RegistroS1.longitud!=None, RegistroS1.latitud \
        !=None, RegistroS1.fecha>=desde, RegistroS1.fecha<=hasta, \
        RegistroS1.estado=='ACTIVO').all()

        for fila_registro in registros:
            insert_str = 'INSERT INTO puntos_visitados('\
            + 'fk_registro_s1, geom) VALUES (:id, ST_SetSRID('\
            + 'ST_Point(:longitud, :latitud), 4326))'

            db_session.execute(text(insert_str), {'id': fila_registro.id, \
            'longitud': fila_registro.longitud, \
            'latitud': fila_registro.latitud})
            db_session.flush()

        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def load_puntos_baldios(desde, hasta):
    delete_str = 'DELETE FROM puntos_baldios'

    try:
        db_session.execute(text(delete_str), {})
        db_session.flush()

        registros = db_session.query(RegistroS1).\
        filter(RegistroS1.longitud!=None, RegistroS1.latitud \
        !=None, RegistroS1.fecha>=desde, RegistroS1.fecha<=hasta, \
        RegistroS1.estado=='ACTIVO', RegistroS1.fk_tipo_predio==2).all()

        for fila_registro in registros:
            insert_str = 'INSERT INTO puntos_baldios('\
            + 'fk_registro_s1, geom) VALUES (:id, ST_SetSRID('\
            + 'ST_Point(:longitud, :latitud), 4326))'

            db_session.execute(text(insert_str), {'id': fila_registro.id, \
            'longitud': fila_registro.longitud, \
            'latitud': fila_registro.latitud})
            db_session.flush()

        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def load_puntos_febriles(desde, hasta):
    delete_str = 'DELETE FROM puntos_febriles'

    try:
        db_session.execute(text(delete_str), {})
        db_session.flush()

        registros = db_session.query(RegistroS1).\
        filter(RegistroS1.longitud!=None, RegistroS1.latitud \
        !=None, RegistroS1.fecha>=desde, RegistroS1.fecha<=hasta, \
        RegistroS1.estado=='ACTIVO', RegistroS1.febriles!=None, \
        RegistroS1.febriles>0).all()

        for fila_registro in registros:
            insert_str = 'INSERT INTO puntos_febriles('\
            + 'fk_registro_s1, geom) VALUES (:id, ST_SetSRID('\
            + 'ST_Point(:longitud, :latitud), 4326))'

            db_session.execute(text(insert_str), {'id': fila_registro.id, \
            'longitud': fila_registro.longitud, \
            'latitud': fila_registro.latitud})
            db_session.flush()

        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def load_puntos_gomerias(desde, hasta):
    delete_str = 'DELETE FROM puntos_gomerias'

    try:
        db_session.execute(text(delete_str), {})
        db_session.flush()

        registros = db_session.query(RegistroS1).\
        filter(RegistroS1.longitud!=None, RegistroS1.latitud \
        !=None, RegistroS1.fecha>=desde, RegistroS1.fecha<=hasta, \
        RegistroS1.estado=='ACTIVO', RegistroS1.fk_tipo_predio==1).all()

        for fila_registro in registros:
            insert_str = 'INSERT INTO puntos_gomerias('\
            + 'fk_registro_s1, geom) VALUES (:id, ST_SetSRID('\
            + 'ST_Point(:longitud, :latitud), 4326))'

            db_session.execute(text(insert_str), {'id': fila_registro.id, \
            'longitud': fila_registro.longitud, \
            'latitud': fila_registro.latitud})
            db_session.flush()

        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def load_puntos_criadero_a(desde, hasta):
    delete_str = 'DELETE FROM puntos_criadero_a'

    try:
        db_session.execute(text(delete_str), {})
        db_session.flush()

        registros = db_session.query(RegistroS1).\
        filter(RegistroS1.longitud!=None, RegistroS1.latitud \
        !=None, RegistroS1.fecha>=desde, RegistroS1.fecha<=hasta, \
        RegistroS1.estado=='ACTIVO').all()

        for fila_registro in registros:
            contador = db_session.query(CriaderoA).filter_by\
            (fk_registro_s1=fila_registro.id).count()

            if contador > 0:
                criadero_a = db_session.query(CriaderoA).filter_by\
                (fk_registro_s1=fila_registro.id).one()

                cantidad = 0

                if criadero_a.criaderos_a1 != None \
                    and criadero_a.criaderos_a1 > 0:
                        cantidad = cantidad + criadero_a.criaderos_a1
                if criadero_a.criaderos_a2 != None \
                    and criadero_a.criaderos_a2 > 0:
                        cantidad = cantidad + criadero_a.criaderos_a2
                if criadero_a.criaderos_a3 != None \
                    and criadero_a.criaderos_a3 > 0:
                        cantidad = cantidad + criadero_a.criaderos_a3
                if criadero_a.criaderos_a4 != None \
                    and criadero_a.criaderos_a4 > 0:
                        cantidad = cantidad + criadero_a.criaderos_a4
                if criadero_a.criaderos_a5 != None \
                    and criadero_a.criaderos_a5 > 0:
                        cantidad = cantidad + criadero_a.criaderos_a5
                if criadero_a.criaderos_a6 != None \
                    and criadero_a.criaderos_a6 > 0:
                        cantidad = cantidad + criadero_a.criaderos_a6
                if criadero_a.criaderos_a7 != None \
                    and criadero_a.criaderos_a7 > 0:
                        cantidad = cantidad + criadero_a.criaderos_a7
                if criadero_a.criaderos_a8 != None \
                    and criadero_a.criaderos_a8 > 0:
                        cantidad = cantidad + criadero_a.criaderos_a8
                if criadero_a.criaderos_a9 != None \
                    and criadero_a.criaderos_a9 > 0:
                        cantidad = cantidad + criadero_a.criaderos_a9
                if criadero_a.criaderos_a10 != None \
                    and criadero_a.criaderos_a10 > 0:
                        cantidad = cantidad + criadero_a.criaderos_a10

                if cantidad > 0:
                    insert_str = 'INSERT INTO puntos_criadero_a('\
                    + 'fk_registro_s1, geom) VALUES (:id, ST_SetSRID('\
                    + 'ST_Point(:longitud, :latitud), 4326))'

                    db_session.execute(text(insert_str), {'id': fila_registro.id, \
                    'longitud': fila_registro.longitud, \
                    'latitud': fila_registro.latitud})
                    db_session.flush()

        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def load_puntos_criadero_b(desde, hasta):
    delete_str = 'DELETE FROM puntos_criadero_b'

    try:
        db_session.execute(text(delete_str), {})
        db_session.flush()

        registros = db_session.query(RegistroS1).\
        filter(RegistroS1.longitud!=None, RegistroS1.latitud \
        !=None, RegistroS1.fecha>=desde, RegistroS1.fecha<=hasta, \
        RegistroS1.estado=='ACTIVO').all()

        for fila_registro in registros:
            contador = db_session.query(CriaderoB).filter_by\
            (fk_registro_s1=fila_registro.id).count()

            if contador > 0:
                criadero_b = db_session.query(CriaderoB).filter_by\
                (fk_registro_s1=fila_registro.id).one()

                cantidad = 0

                if criadero_b.criaderos_b1 != None \
                    and criadero_b.criaderos_b1 > 0:
                        cantidad = cantidad + criadero_b.criaderos_b1
                if criadero_b.criaderos_b2 != None \
                    and criadero_b.criaderos_b2 > 0:
                        cantidad = cantidad + criadero_b.criaderos_b2
                if criadero_b.criaderos_b3 != None \
                    and criadero_b.criaderos_b3 > 0:
                        cantidad = cantidad + criadero_b.criaderos_b3
                if criadero_b.criaderos_b4 != None \
                    and criadero_b.criaderos_b4 > 0:
                        cantidad = cantidad + criadero_b.criaderos_b4
                if criadero_b.criaderos_b5 != None \
                    and criadero_b.criaderos_b5 > 0:
                        cantidad = cantidad + criadero_b.criaderos_b5
                if criadero_b.criaderos_b6 != None \
                    and criadero_b.criaderos_b6 > 0:
                        cantidad = cantidad + criadero_b.criaderos_b6

                if cantidad > 0:
                    insert_str = 'INSERT INTO puntos_criadero_b('\
                    + 'fk_registro_s1, geom) VALUES (:id, ST_SetSRID('\
                    + 'ST_Point(:longitud, :latitud), 4326))'

                    db_session.execute(text(insert_str), {'id': fila_registro.id, \
                    'longitud': fila_registro.longitud, \
                    'latitud': fila_registro.latitud})
                    db_session.flush()

        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def load_puntos_criadero_c(desde, hasta):
    delete_str = 'DELETE FROM puntos_criadero_c'

    try:
        db_session.execute(text(delete_str), {})
        db_session.flush()

        registros = db_session.query(RegistroS1).\
        filter(RegistroS1.longitud!=None, RegistroS1.latitud \
        !=None, RegistroS1.fecha>=desde, RegistroS1.fecha<=hasta, \
        RegistroS1.estado=='ACTIVO').all()

        for fila_registro in registros:
            contador = db_session.query(CriaderoC).filter_by\
            (fk_registro_s1=fila_registro.id).count()

            if contador > 0:
                criadero_c = db_session.query(CriaderoC).filter_by\
                (fk_registro_s1=fila_registro.id).one()

                cantidad = 0

                if criadero_c.criaderos_c1 != None \
                    and criadero_c.criaderos_c1 > 0:
                        cantidad = cantidad + criadero_c.criaderos_c1
                if criadero_c.criaderos_c2 != None \
                    and criadero_c.criaderos_c2 > 0:
                        cantidad = cantidad + criadero_c.criaderos_c2
                if criadero_c.criaderos_c3 != None \
                    and criadero_c.criaderos_c3 > 0:
                        cantidad = cantidad + criadero_c.criaderos_c3
                if criadero_c.criaderos_c4 != None \
                    and criadero_c.criaderos_c4 > 0:
                        cantidad = cantidad + criadero_c.criaderos_c4

                if cantidad > 0:
                    insert_str = 'INSERT INTO puntos_criadero_c('\
                    + 'fk_registro_s1, geom) VALUES (:id, ST_SetSRID('\
                    + 'ST_Point(:longitud, :latitud), 4326))'

                    db_session.execute(text(insert_str), {'id': fila_registro.id, \
                    'longitud': fila_registro.longitud, \
                    'latitud': fila_registro.latitud})
                    db_session.flush()

        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def load_puntos(desde, hasta):
    _desde = datetime.datetime.strptime(desde, '%d/%m/%Y')
    _hasta = datetime.datetime.strptime(hasta, '%d/%m/%Y')

    resultado = load_puntos_visitados(_desde, _hasta)

    if resultado != 'OK':
        return resultado

    resultado = load_puntos_baldios(_desde, _hasta)

    if resultado != 'OK':
        return resultado

    resultado = load_puntos_febriles(_desde, _hasta)

    if resultado != 'OK':
        return resultado

    resultado = load_puntos_gomerias(_desde, _hasta)

    if resultado != 'OK':
        return resultado

    resultado = load_puntos_criadero_a(_desde, _hasta)

    if resultado != 'OK':
        return resultado

    resultado = load_puntos_criadero_b(_desde, _hasta)

    if resultado != 'OK':
        return resultado

    resultado = load_puntos_criadero_c(_desde, _hasta)

    if resultado != 'OK':
        return resultado

    return 'OK'
