# Import flask dependencies
from flask import Blueprint
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from functools import wraps

from app import login_required
from app.mod_mapa import controllers
from app.mod_auth import controllers as ctrl_auth

import time

def _get_unauthorized_view():
    return redirect(url_for('no_autorizado'))

def roles_required(*roles):
    """Decorador que especifica que un usuario debe tener todos los 
    roles especificados."""
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            id_usuario = session['id_usuario']
            continuar = ctrl_auth.roles_required(id_usuario, roles)
            if(continuar):
                return fn(*args, **kwargs)
            return _get_unauthorized_view()
        return decorated_view
    return wrapper

def roles_accepted(*roles):
    """Decorador que especifica que un usuario debe tener al menos uno
    de los roles especificados"""
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            id_usuario = session['id_usuario']
            continuar = ctrl_auth.roles_accepted(id_usuario, roles)
            if(continuar):
                return fn(*args, **kwargs)
            return _get_unauthorized_view()
        return decorated_view
    return wrapper

# Define the blueprint
mod_mapa = Blueprint('mapa', __name__, url_prefix='/mapa')

# Set the route and accepted methods
@mod_mapa.route('/puntos_visitados', methods=['GET', 'POST'])
@login_required
@roles_accepted('PTOVISIT')
def puntos_visitados():
    error = None
    id_usuario = session['id_usuario']

    fecha_desde = time.strftime('%d/%m/%Y')
    fecha_hasta = time.strftime('%d/%m/%Y')

    if request.method == 'POST':
        if request.form["action"] == "Salir":
            return redirect(url_for('auth.menu_principal'))
        if request.form["action"] == "VerMapa":
            fecha_desde = request.form["fecha_desde"]
            fecha_hasta = request.form["fecha_hasta"]
            error = controllers.load_puntos(fecha_desde, fecha_hasta)
            if error == 'OK':
                error = None
    else:
        error = controllers.load_puntos(fecha_desde, fecha_hasta)
        if error == 'OK':
            error = None

    return render_template('mapa/puntos_visitados.html', error=error, \
    id_usuario=id_usuario, fecha_desde=fecha_desde, fecha_hasta=\
    fecha_hasta)
