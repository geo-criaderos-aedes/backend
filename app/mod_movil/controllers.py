from database import db_session

# Import module models
from app.mod_user.models import Usuario
from app.mod_tbls1.models import TipoPredio, TipoControl, TablaS1, \
RegistroS1, CriaderoA, CriaderoB, CriaderoC, Departamento, Distrito, Barrio

from werkzeug.security import generate_password_hash, \
check_password_hash

from sqlalchemy import exc
from sqlalchemy import or_

from sqlalchemy.sql.expression import func

import datetime
from sqlalchemy.orm import aliased
from sqlalchemy import cast, DATE

def set_password(password):
    return generate_password_hash(password)

def check_password(pw_hash, password):
    return check_password_hash(pw_hash, password)

def verificar_login(username, password):
    try:
        contador = db_session.query(Usuario). \
        filter_by(id_usuario=username, estado='ACTIVO').count()

        if contador == 0:
            return contador

        usuario = db_session.query(Usuario).\
        filter_by(id_usuario=username).one()

        usuario_contrasena = usuario.contrasena
        aux = check_password(usuario_contrasena, password)

        if(aux==True):
            return usuario.id
        else:
            return 0
    except exc.SQLAlchemyError:
        return 0

def verificar_datos_requeridos_alta(id_usuario, nombre, apellido, \
contrasena, contrasena2, fecha_nac):
    sw = True

    if id_usuario == "":
        sw = False
    elif contrasena == "":
        sw = False
    elif contrasena2 == "":
        sw = False
    elif nombre == "":
        sw = False
    elif apellido == "":
        sw = False
    elif fecha_nac == "":
        sw = False
    return sw

def verificar_contrasena(pass1, pass2):
    return pass1 == pass2

def insertar_usuario(id_usuario, contrasena, nombre, apellido, \
fecha_nacimiento, usuario_alta):
    contador = db_session.query(Usuario). \
    filter_by(id_usuario=id_usuario).count()

    if contador > 0:
        return 'El usuario ya existe'

    try:
        usuario = Usuario()
        usuario.id_usuario = id_usuario
        usuario.contrasena = set_password(contrasena)
        usuario.nombre = nombre
        usuario.apellido = apellido
        usuario.fecha_nacimiento = fecha_nacimiento
        usuario.fecha_creacion = datetime.datetime.now()
        usuario.usuario_creacion = usuario_alta
        usuario.fecha_modificacion = datetime.datetime.now()
        usuario.usuario_modificacion = usuario_alta
        usuario.estado = 'ACTIVO'
        db_session.add(usuario)
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return "OK"

def listar_tipos_controles():
    tipos_controles = db_session.query(TipoControl).all()
    items = []
    for fila_tipo_control in tipos_controles:
        item = dict(id=fila_tipo_control.id, \
        nombre=fila_tipo_control.nombre)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def listar_tipos_predios():
    tipos_predios = db_session.query(TipoPredio).all()
    items = []
    for fila_tipo_predio in tipos_predios:
        item = dict(id=fila_tipo_predio.id, \
        nombre=fila_tipo_predio.nombre)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def verificar_datos_requeridos_tabla_s1(fk_tipo_control, fk_barrio, \
fk_encargado, fk_supervisor, fk_operador, cuadrilla, brigada, fk_usuario):
    sw = True

    if fk_tipo_control == "":
        sw = False
    elif fk_barrio == "":
        sw = False
    elif fk_encargado == "":
        sw = False
    elif fk_supervisor == "":
        sw = False
    elif fk_operador == "":
        sw = False
    elif cuadrilla == "":
        sw = False
    elif brigada == "":
        sw = False
    elif fk_usuario == "":
        sw = False
    return sw

def insertar_tabla_s1(fk_tipo_control, fk_barrio, fk_encargado, \
fk_supervisor, fk_operador, cuadrilla, brigada, fk_usuario):

    fecha = datetime.datetime.now().strftime("%Y-%m-%d")

    contador = db_session.query(TablaS1). \
    filter_by(fk_tipo_control=fk_tipo_control, fk_barrio=fk_barrio, \
    cuadrilla=cuadrilla, brigada=brigada, fecha=fecha, estado='ACTIVO', fk_usuario=fk_usuario).count()

    if contador > 0:
        print 'La planilla ya existe'
        return 'La planilla ya existe'

    try:
        tabla_s1 = TablaS1()
        tabla_s1.fk_tipo_control = fk_tipo_control
        tabla_s1.fk_barrio = fk_barrio
        tabla_s1.fk_encargado = fk_encargado
        tabla_s1.fk_supervisor = fk_supervisor
        tabla_s1.fk_operador = fk_operador
        tabla_s1.cuadrilla = cuadrilla
        tabla_s1.brigada = brigada
        tabla_s1.fk_usuario = fk_usuario
        tabla_s1.fecha = fecha
        tabla_s1.estado = 'ACTIVO'
        db_session.add(tabla_s1)
        db_session.flush()
        db_session.commit()
        
        return tabla_s1.id
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

def obtener_id(id_usuario):
    item = None
    contador = db_session.query(Usuario).filter_by(id_usuario=id_usuario).count()

    if contador > 0:
        usr = db_session.query(Usuario).filter_by(id_usuario=id_usuario).one()

        item = usr.id

    return item

def listar_usuarios():
    usuarios = db_session.query(Usuario).all()
    items = []
    for fila_usuario in usuarios:
        item = dict(id=fila_usuario.id, \
        id_usuario=fila_usuario.id_usuario, nombre=fila_usuario.nombre, \
        apellido=fila_usuario.apellido, estado=fila_usuario.estado)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['id_usuario'])
    return items
    
def insertar_registro( \
p_fk_tabla_s1, p_manzana, p_direccion, p_latitud, \
p_longitud, p_verificado, p_fk_tipo_predio, p_jefe_familia, \
p_habitantes, p_febriles, p_sin_criaderos, p_criaderos_eliminados, \
p_tratados_fisicamente, p_tratados_larvicida, p_larvicida_a, \
#p_larvicida_b, p_larvicida_c, 
p_criaderos_a1, \
p_criaderos_a2, p_criaderos_a3, p_criaderos_a4, p_criaderos_a5, \
p_criaderos_a6, p_criaderos_a7, p_criaderos_a8, p_criaderos_a9, \
p_criaderos_a10, p_criaderos_b1, p_criaderos_b2, \
p_criaderos_b3, p_criaderos_b4, p_criaderos_b5, p_criaderos_b6, \
p_criaderos_c1, p_criaderos_c2,\
p_criaderos_c3, p_criaderos_c4, p_sw_criadero_a, p_sw_criadero_b, \
p_sw_criadero_c):

    p_fecha = datetime.datetime.now().strftime("%Y-%m-%d")

    contador = db_session.query(RegistroS1). \
    filter_by(fk_tabla_s1=p_fk_tabla_s1, manzana=p_manzana, \
    fk_tipo_predio=p_fk_tipo_predio, direccion=p_direccion, fecha=p_fecha, estado='ACTIVO').count()

    if contador > 0:
        print 'El registro ya existe'
        return 'El registro ya existe'
        

    try:
        
        registro_s1 = RegistroS1()
        registro_s1.fk_tabla_s1 = p_fk_tabla_s1
        registro_s1.manzana = p_manzana
        registro_s1.direccion = p_direccion
        registro_s1.latitud = p_latitud
        registro_s1.longitud = p_longitud
        registro_s1.verificado = p_verificado
        registro_s1.fk_tipo_predio = p_fk_tipo_predio
        registro_s1.jefe_familia = p_jefe_familia
        registro_s1.habitantes = p_habitantes
        registro_s1.febriles = p_febriles
        registro_s1.sin_criaderos = p_sin_criaderos
        registro_s1.criaderos_eliminados = p_criaderos_eliminados
        registro_s1.tratados_fisicamente = p_tratados_fisicamente
        registro_s1.tratados_larvicida = p_tratados_larvicida
        registro_s1.larvicida_a = p_larvicida_a
        #registro_s1.larvicida_b = p_larvicida_b
        #registro_s1.larvicida_c = p_larvicida_c
        registro_s1.fecha = p_fecha
        registro_s1.estado = 'ACTIVO'
        db_session.add(registro_s1)
        db_session.flush()
        db_session.commit()
        
        p_id = registro_s1.id
        
        if p_sw_criadero_a == True:
            criadero_a = CriaderoA()
            criadero_a.fk_registro_s1 = p_id
            criadero_a.criaderos_a1 = p_criaderos_a1
            criadero_a.criaderos_a2 = p_criaderos_a2
            criadero_a.criaderos_a3 = p_criaderos_a3
            criadero_a.criaderos_a4 = p_criaderos_a4
            criadero_a.criaderos_a5 = p_criaderos_a5
            criadero_a.criaderos_a6 = p_criaderos_a6
            criadero_a.criaderos_a7 = p_criaderos_a7
            criadero_a.criaderos_a8 = p_criaderos_a8
            criadero_a.criaderos_a9 = p_criaderos_a9
            criadero_a.criaderos_a10 = p_criaderos_a10
            db_session.add(criadero_a)
            db_session.flush()
            db_session.commit()
            
        if p_sw_criadero_b == True:

            criadero_b = CriaderoB()
            criadero_b.fk_registro_s1 = p_id
            criadero_b.criaderos_b1 = p_criaderos_b1
            criadero_b.criaderos_b2 = p_criaderos_b2
            criadero_b.criaderos_b3 = p_criaderos_b3
            criadero_b.criaderos_b4 = p_criaderos_b4
            criadero_b.criaderos_b5 = p_criaderos_b5
            criadero_b.criaderos_b6 = p_criaderos_b6
            db_session.add(criadero_b)
            db_session.flush()
            db_session.commit()
        
        if p_sw_criadero_c == True:
        
            criadero_c = CriaderoC()
            criadero_c.fk_registro_s1 = p_id
            criadero_c.criaderos_c1 = p_criaderos_c1
            criadero_c.criaderos_c2 = p_criaderos_c2
            criadero_c.criaderos_c3 = p_criaderos_c3
            criadero_c.criaderos_c4 = p_criaderos_c4
            db_session.add(criadero_c)
            db_session.flush()
            db_session.commit()
        
        print registro_s1.id
        return registro_s1.id
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        print e.message
        return e.message
    
def verificar_datos_requeridos_registro( \
p_manzana, p_direccion, p_verificado, p_fk_tipo_predio):
    sw = True

    if p_manzana == "":
        sw = False
    elif p_direccion == "":
        sw = False
    elif p_verificado == None:
        sw = False
    elif p_fk_tipo_predio == None:
        sw = False
    return sw
    
def verificar_datos_requeridos_criadero_a( \
p_criaderos_a1, p_criaderos_a2, p_criaderos_a3, \
p_criaderos_a4, p_criaderos_a5, p_criaderos_a6, p_criaderos_a7, \
p_criaderos_a8, p_criaderos_a9, p_criaderos_a10):
    sw = False

    if p_criaderos_a1 != None or p_criaderos_a1 != '':
        sw = True
    elif p_criaderos_a2 != None or p_criaderos_a2 != '':
        sw = True
    elif p_criaderos_a3 != None or p_criaderos_a3 != '':
        sw = True
    elif p_criaderos_a4 != None or p_criaderos_a4 != '':
        sw = True
    elif p_criaderos_a5 != None or p_criaderos_a5 != '':
        sw = True
    elif p_criaderos_a6 != None or p_criaderos_a6 != '':
        sw = True
    elif p_criaderos_a7 != None or p_criaderos_a7 != '':
        sw = True
    elif p_criaderos_a8 != None or p_criaderos_a8 != '':
        sw = True
    elif p_criaderos_a9 != None or p_criaderos_a9 != '':
        sw = True
    elif p_criaderos_a10 != None or p_criaderos_a10 != '':
        sw = True
    return sw
    
def verificar_datos_requeridos_criadero_b(\
p_criaderos_b1, p_criaderos_b2, p_criaderos_b3, \
p_criaderos_b4, p_criaderos_b5, p_criaderos_b6):
    sw = False

    if p_criaderos_b1 != None or p_criaderos_b1 != '':
        sw = True
    elif p_criaderos_b2 != None or p_criaderos_b2 != '':
        sw = True
    elif p_criaderos_b3 != None or p_criaderos_b3 != '':
        sw = True
    elif p_criaderos_b4 != None or p_criaderos_b4 != '':
        sw = True
    elif p_criaderos_b5 != None or p_criaderos_b5 != '':
        sw = True
    elif p_criaderos_b6 != None or p_criaderos_b6 != '':
        sw = True
    return sw
    
def verificar_datos_requeridos_criadero_c(\
p_criaderos_c1, p_criaderos_c2, p_criaderos_c3, \
p_criaderos_c4):
    sw = False

    if p_criaderos_c1 != None or p_criaderos_c1 != '':
        sw = True
    elif p_criaderos_c2 != None or p_criaderos_c2 != '':
        sw = True
    elif p_criaderos_c3 != None or p_criaderos_c3 != '':
        sw = True
    elif p_criaderos_c4 != None or p_criaderos_c4 != '':
        sw = True
    return sw
    
def listar_tabla_s1():
    Usuario0 = aliased(Usuario)
    Usuario1 = aliased(Usuario)
    Usuario2 = aliased(Usuario)
    Usuario3 = aliased(Usuario)
    p_fecha = datetime.datetime.now().strftime("%Y-%m-%d")
    items = []

    tablas_s1 = db_session.query(TablaS1.id, TipoControl.nombre.label('tipo_control'), \
    Departamento.nombre.label('departamento'), Distrito.nombre.label('distrito'), \
    Barrio.nombre.label('barrio'), Usuario0.nombre.label('encargado'), 
    Usuario1.nombre.label('supervisor'), Usuario2.nombre.label('operador'), 
    Usuario3.nombre.label('usuario'), TablaS1.cuadrilla, TablaS1.brigada, TablaS1.estado, TablaS1.fecha ).\
    join(TipoControl, TablaS1.fk_tipo_control == TipoControl.id).join(Barrio, TablaS1.fk_barrio == Barrio.id).\
    join(Usuario0, TablaS1.fk_encargado == Usuario0.id).join(Usuario1, TablaS1.fk_supervisor == Usuario1.id).\
    join(Usuario2, TablaS1.fk_operador == Usuario2.id).join(Usuario3, TablaS1.fk_usuario == Usuario3.id).\
    join(Distrito, Barrio.fk_distrito == Distrito.id).join(Departamento, Distrito.fk_departamento == Departamento.id).\
    filter(TablaS1.estado == 'ACTIVO', TablaS1.fecha == p_fecha).all()

    for fila_tabla_s1 in tablas_s1:
        item = dict(id=fila_tabla_s1.id, tipo_control= fila_tabla_s1.tipo_control, \
        departamento = fila_tabla_s1.departamento, distrito = fila_tabla_s1.distrito,\
        barrio = fila_tabla_s1.barrio, encargado = fila_tabla_s1.encargado, \
        supervisor = fila_tabla_s1.supervisor, operador = fila_tabla_s1.operador, \
        cuadrilla = fila_tabla_s1.cuadrilla, brigada = fila_tabla_s1.brigada, \
        usuario = fila_tabla_s1.usuario, fecha = fila_tabla_s1.fecha.strftime("%Y-%m-%d"), \
        estado = fila_tabla_s1.estado)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['fecha'])
    return items
    
def buscar_tabla_s1(id_s1):
    item = None
    Usuario0 = aliased(Usuario)
    Usuario1 = aliased(Usuario)
    Usuario2 = aliased(Usuario)
    Usuario3 = aliased(Usuario)

    tabla_s1 = db_session.query(TablaS1.id, TipoControl.id.label('tipo_control'), \
    Departamento.id.label('departamento'), Distrito.id.label('distrito'), \
    Barrio.id.label('barrio'), Usuario0.id.label('encargado'), 
    Usuario1.id.label('supervisor'), Usuario2.id.label('operador'), 
    Usuario3.id.label('usuario'), TablaS1.cuadrilla, TablaS1.brigada, TablaS1.estado, TablaS1.fecha ).\
    join(TipoControl, TablaS1.fk_tipo_control == TipoControl.id).join(Barrio, TablaS1.fk_barrio == Barrio.id).\
    join(Usuario0, TablaS1.fk_encargado == Usuario0.id).join(Usuario1, TablaS1.fk_supervisor == Usuario1.id).\
    join(Usuario2, TablaS1.fk_operador == Usuario2.id).join(Usuario3, TablaS1.fk_usuario == Usuario3.id).\
    join(Distrito, Barrio.fk_distrito == Distrito.id).join(Departamento, Distrito.fk_departamento == Departamento.id).\
    filter(TablaS1.estado == 'ACTIVO', TablaS1.id == id_s1).one()

    item = dict(id=tabla_s1.id, fk_tipo_control= tabla_s1.tipo_control, \
    fk_distrito = tabla_s1.distrito, fk_departamento = tabla_s1.departamento, \
    fk_barrio = tabla_s1.barrio, fk_encargado = tabla_s1.encargado, \
    fk_supervisor = tabla_s1.supervisor, fk_operador = tabla_s1.operador, \
    cuadrilla = tabla_s1.cuadrilla, brigada = tabla_s1.brigada, \
    fk_usuario = tabla_s1.usuario, fecha = tabla_s1.fecha, \
    estado = tabla_s1.estado)

    return item
    
def modificar_tabla_s1(p_id, p_fk_tipo_control, p_fk_barrio, p_fk_encargado, \
p_fk_supervisor, p_fk_operador, p_cuadrilla, p_brigada, p_fk_usuario):

    p_fecha = datetime.datetime.now().strftime("%Y-%m-%d")

    contador = db_session.query(TablaS1). \
    filter_by(id = p_id).count()

    if contador == 0:
        return 'La planilla no existe'

    try:
        tabla_s1 = db_session.query(TablaS1).filter_by(id=p_id).one()
        
        tabla_s1.fk_tipo_control = p_fk_tipo_control
        tabla_s1.fk_barrio = p_fk_barrio
        tabla_s1.fk_encargado = p_fk_encargado
        tabla_s1.fk_supervisor = p_fk_supervisor
        tabla_s1.fk_operador = p_fk_operador
        tabla_s1.cuadrilla = p_cuadrilla
        tabla_s1.brigada = p_brigada
        tabla_s1.fk_usuario = p_fk_usuario
        tabla_s1.fecha = p_fecha
        db_session.add(tabla_s1)
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return "OK"
    
def modificar_registro( \
p_id, p_fk_tabla_s1, p_manzana, p_direccion, \
p_verificado, p_fk_tipo_predio, p_jefe_familia, \
p_habitantes, p_febriles, p_sin_criaderos, p_criaderos_eliminados, \
p_tratados_fisicamente, p_tratados_larvicida, p_larvicida_a, \
#p_larvicida_b, p_larvicida_c, 
p_criaderos_a1, \
p_criaderos_a2, p_criaderos_a3, p_criaderos_a4, p_criaderos_a5, \
p_criaderos_a6, p_criaderos_a7, p_criaderos_a8, p_criaderos_a9, \
p_criaderos_a10, p_criaderos_b1, p_criaderos_b2, \
p_criaderos_b3, p_criaderos_b4, p_criaderos_b5, p_criaderos_b6, \
p_criaderos_c1, p_criaderos_c2,\
p_criaderos_c3, p_criaderos_c4):

    p_fecha = datetime.datetime.now().strftime("%Y-%m-%d")

    contador = db_session.query(RegistroS1).filter_by(id=p_id).count()

    if contador == 0:
        return 'El registro no existe'
        
    try:
        
        registro_s1 = db_session.query(RegistroS1). \
        filter_by(id=p_id).one()
        
        registro_s1.manzana = p_manzana
        registro_s1.direccion = p_direccion
        registro_s1.verificado = p_verificado
        registro_s1.fk_tipo_predio = p_fk_tipo_predio
        registro_s1.jefe_familia = p_jefe_familia
        registro_s1.habitantes = p_habitantes
        registro_s1.febriles = p_febriles
        registro_s1.sin_criaderos = p_sin_criaderos
        registro_s1.criaderos_eliminados = p_criaderos_eliminados
        registro_s1.tratados_fisicamente = p_tratados_fisicamente
        registro_s1.tratados_larvicida = p_tratados_larvicida
        registro_s1.larvicida_a = p_larvicida_a
        #registro_s1.larvicida_b = p_larvicida_b
        #registro_s1.larvicida_c = p_larvicida_c
        registro_s1.fecha = p_fecha
        db_session.add(registro_s1)
        db_session.flush()
        db_session.commit()
        
        p_sw_criadero_a = db_session.query(CriaderoA).filter_by(fk_registro_s1=p_id).count()
        p_sw_criadero_b = db_session.query(CriaderoB).filter_by(fk_registro_s1=p_id).count()
        p_sw_criadero_c = db_session.query(CriaderoC).filter_by(fk_registro_s1=p_id).count()
        
        if p_sw_criadero_a > 0:
            
            criadero_a = db_session.query(CriaderoA).filter_by( \
            fk_registro_s1=p_id).one()
        
            criadero_a.criaderos_a1 = p_criaderos_a1
            criadero_a.criaderos_a2 = p_criaderos_a2
            criadero_a.criaderos_a3 = p_criaderos_a3
            criadero_a.criaderos_a4 = p_criaderos_a4
            criadero_a.criaderos_a5 = p_criaderos_a5
            criadero_a.criaderos_a6 = p_criaderos_a6
            criadero_a.criaderos_a7 = p_criaderos_a7
            criadero_a.criaderos_a8 = p_criaderos_a8
            criadero_a.criaderos_a9 = p_criaderos_a9
            criadero_a.criaderos_a10 = p_criaderos_a10
            db_session.add(criadero_a)
            db_session.flush()
            db_session.commit()
        
        if p_sw_criadero_b > 0:
            
            criadero_b = db_session.query(CriaderoB).filter_by( \
            fk_registro_s1=p_id).one()

            criadero_b.criaderos_b1 = p_criaderos_b1
            criadero_b.criaderos_b2 = p_criaderos_b2
            criadero_b.criaderos_b3 = p_criaderos_b3
            criadero_b.criaderos_b4 = p_criaderos_b4
            criadero_b.criaderos_b5 = p_criaderos_b5
            criadero_b.criaderos_b6 = p_criaderos_b6
            db_session.add(criadero_b)
            db_session.flush()
            db_session.commit()
        
        if p_sw_criadero_c > 0:
            
            criadero_c = db_session.query(CriaderoC).filter_by( \
            fk_registro_s1=p_id).one()
        
            criadero_c.criaderos_c1 = p_criaderos_c1
            criadero_c.criaderos_c2 = p_criaderos_c2
            criadero_c.criaderos_c3 = p_criaderos_c3
            criadero_c.criaderos_c4 = p_criaderos_c4
            db_session.add(criadero_c)
            db_session.flush()
            db_session.commit()
            
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message
        
    return "OK"
    
def listar_departamentos():
    items = []

    departamentos = db_session.query(Departamento).all()

    for fila_departamento in departamentos:
        item = dict(id=fila_departamento.id, \
        nombre=fila_departamento.nombre)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items
    
def listar_distritos(departamento):
    if departamento != '0':
        distritos = db_session.query(Distrito).\
        filter_by(fk_departamento=departamento).all()
    else:
        distritos = db_session.query(Distrito).all()

    items = []
    for fila_distrito in distritos:
        item = dict(id=fila_distrito.id, \
        fk_departamento=fila_distrito.fk_departamento, nombre=fila_distrito.nombre)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items
    
def listar_barrios(distrito):
    if distrito != '0':
        barrios = db_session.query(Barrio).\
        filter_by(fk_distrito=distrito).all()
    else:
        barrios = db_session.query(Barrio).all()

    items = []
    for fila_barrio in barrios:
        item = dict(id=fila_barrio.id, \
        fk_distrito=fila_barrio.fk_distrito, nombre=fila_barrio.nombre)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items
    
def verificar_datos_requeridos_contrasena(passanterior, passnueva, passnueva2):
    sw = True

    if passanterior == "" or passanterior == None:
        sw = False
    elif passnueva == "" or passnueva == None:
        sw = False
    elif passnueva2 == "" or passnueva2 == None:
        sw = False
    return sw

def modificar_contrasena(id_usuario, passanterior, passnueva, passnueva2):
    try:
        usuario = db_session.query(Usuario).\
        filter_by(id_usuario=id_usuario).one()

        usuario_contrasena = usuario.contrasena

        if not check_password(usuario_contrasena, passanterior):
            return u'La contrase\xf1a anterior no es correcta'

        if passnueva != passnueva2:
            return u'Las contrase\xf1as nuevas ingresadas no son iguales'

        usuario.contrasena = set_password(passnueva)
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'
