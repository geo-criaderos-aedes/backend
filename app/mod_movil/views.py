# Import flask dependencies
from flask import Blueprint
from flask import request
from flask import jsonify

from app.mod_movil import controllers as controllers_movil
from app.mod_tbls1 import controllers as controllers_tbls1

# Define the blueprint
mod_movil = Blueprint('movil', __name__, url_prefix='/movil')

# Set the route and accepted methods
@mod_movil.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        data = request.json
        username = data['username']
        password = data['password']
        validacion = controllers_movil.verificar_login(username, password)
        return jsonify(respuesta=validacion)

@mod_movil.route('/departamentos')
def departamentos():
    departamentos = controllers_tbls1.listar_departamentos()
    return jsonify(departamentos)

@mod_movil.route('/distritos/<departamento>')
def distritos(departamento):
    distritos = controllers_movil.listar_distritos(departamento)
    return jsonify(distritos)

@mod_movil.route('/barrios/<distrito>')
def barrios(distrito):
    barrios = controllers_movil.listar_barrios(distrito)
    return jsonify(barrios)

@mod_movil.route('/tiposcontroles')
def tiposcontroles():
    tipos_controles = controllers_movil.listar_tipos_controles()
    return jsonify(tipos_controles)

@mod_movil.route('/tipospredios')
def tipospredios():
    tipos_predios = controllers_movil.listar_tipos_predios()
    return jsonify(tipos_predios)

@mod_movil.route('/usuarios/alta', methods=['POST'])
def crear_usuarios():
    if request.method == 'POST':
        data = request.json
        p_usuario = data['id_usuario']
        p_contrasena = data['contrasena']
        p_contrasena2 = data['contrasena2']
        p_nombre = data['nombre']
        p_apellido = data['apellido']
        p_fecha_nacimiento = data['fecha_nacimiento']

        if not controllers_movil.verificar_datos_requeridos_alta(p_usuario, \
        p_nombre, p_apellido, p_contrasena, p_contrasena2, p_fecha_nacimiento):
            return jsonify(respuesta = "Datos requeridos no ingresados")

        if not controllers_movil.verificar_contrasena(p_contrasena, \
        p_contrasena2):
            return jsonify(respuesta = u"Las contrase\xf1as no coinciden")

        id_usr_creador = 1
        return jsonify(respuesta = controllers_movil.insertar_usuario(p_usuario, \
        p_contrasena, p_nombre, p_apellido, p_fecha_nacimiento, \
        id_usr_creador))

@mod_movil.route('/tablas1/alta', methods=['POST'])
def crear_tabla_s1():
    if request.method == 'POST':
        data = request.json
        p_fk_tipo_control = data['fk_tipo_control']
        p_fk_barrio = data['fk_barrio']
        p_fk_encargado = data['fk_encargado']
        p_fk_supervisor = data['fk_supervisor']
        p_fk_operador = data['fk_operador']
        p_cuadrilla = data['cuadrilla']
        p_brigada = data['brigada']
        p_fk_usuario = data['fk_usuario']

        if not controllers_movil.verificar_datos_requeridos_tabla_s1(\
        p_fk_tipo_control, p_fk_barrio, p_fk_encargado, p_fk_supervisor, \
        p_fk_operador, p_cuadrilla, p_brigada, p_fk_usuario):
            return jsonify(respuesta = "Datos requeridos no ingresados")
        
        return jsonify(respuesta = controllers_movil.insertar_tabla_s1(\
        p_fk_tipo_control, p_fk_barrio, p_fk_encargado, p_fk_supervisor, \
        p_fk_operador, p_cuadrilla, p_brigada, p_fk_usuario))

@mod_movil.route('/usuarios')
def usuarios():
    usuarios = controllers_movil.listar_usuarios()
    return jsonify(usuarios)
    
@mod_movil.route('/registro/alta', methods=['POST'])
def crear_registro():
    p_sw_criadero_a = False
    p_sw_criadero_b = False
    p_sw_criadero_c = False
    
    if request.method == 'POST':
        data = request.json
        p_fk_tabla_s1 = data['fk_tabla_s1']
        p_manzana = data['manzana']
        p_direccion = data['direccion']
        p_latitud = data['latitud']
        p_longitud = data['longitud']
        p_verificado = data['verificado']
        p_fk_tipo_predio = data['fk_tipo_predio']
        p_jefe_familia = data['jefe_familia']
        p_habitantes = data['habitantes']
        p_febriles = data['febriles']
        p_sin_criaderos = data['sin_criaderos']
        p_criaderos_eliminados = data['criaderos_eliminados']
        p_tratados_fisicamente = data['tratados_fisicamente']
        p_tratados_larvicida = data['tratados_larvicida']
        p_larvicida_a = data['larvicida_a']
        #p_larvicida_b = data['larvicida_b']
        #p_larvicida_c = data['larvicida_c']
        p_criaderos_a1 = data['criaderos_a1']
        p_criaderos_a2 = data['criaderos_a2']
        p_criaderos_a3 = data['criaderos_a3']
        p_criaderos_a4 = data['criaderos_a4']
        p_criaderos_a5 = data['criaderos_a5']
        p_criaderos_a6 = data['criaderos_a6']
        p_criaderos_a7 = data['criaderos_a7']
        p_criaderos_a8 = data['criaderos_a8']
        p_criaderos_a9 = data['criaderos_a9']
        p_criaderos_a10 = data['criaderos_a10']
        p_criaderos_b1 = data['criaderos_b1']
        p_criaderos_b2 = data['criaderos_b2']
        p_criaderos_b3 = data['criaderos_b3']
        p_criaderos_b4 = data['criaderos_b4']
        p_criaderos_b5 = data['criaderos_b5']
        p_criaderos_b6 = data['criaderos_b6']
        p_criaderos_c1 = data['criaderos_c1']
        p_criaderos_c2 = data['criaderos_c2']
        p_criaderos_c3 = data['criaderos_c3']
        p_criaderos_c4 = data['criaderos_c4']
        
        if p_habitantes == '':
            p_habitantes = None
        if p_febriles == '':
            p_febriles = None

        if not controllers_movil.verificar_datos_requeridos_registro( \
        p_manzana, p_direccion, p_verificado, p_fk_tipo_predio):
            return jsonify(respuesta = "Datos requeridos no ingresados")

        if controllers_movil.verificar_datos_requeridos_criadero_a( \
        p_criaderos_a1, p_criaderos_a2, p_criaderos_a3, \
        p_criaderos_a4, p_criaderos_a5, p_criaderos_a6, p_criaderos_a7, \
        p_criaderos_a8, p_criaderos_a9, p_criaderos_a10):
            p_sw_criadero_a = True
            
        if controllers_movil.verificar_datos_requeridos_criadero_b( \
        p_criaderos_b1, p_criaderos_b2, p_criaderos_b3, \
        p_criaderos_b4, p_criaderos_b5, p_criaderos_b6):
            p_sw_criadero_b = True
            
        if controllers_movil.verificar_datos_requeridos_criadero_c( \
        p_criaderos_c1, p_criaderos_c2, p_criaderos_c3, \
        p_criaderos_c4):
            p_sw_criadero_c = True

        return jsonify(respuesta = controllers_movil.insertar_registro( \
        p_fk_tabla_s1, p_manzana, p_direccion, p_latitud, \
        p_longitud, p_verificado, p_fk_tipo_predio, p_jefe_familia, \
        p_habitantes, p_febriles, p_sin_criaderos, p_criaderos_eliminados, \
        p_tratados_fisicamente, p_tratados_larvicida, p_larvicida_a, \
        #p_larvicida_b, p_larvicida_c, 
        p_criaderos_a1, \
        p_criaderos_a2, p_criaderos_a3, p_criaderos_a4, p_criaderos_a5, \
        p_criaderos_a6, p_criaderos_a7, p_criaderos_a8, p_criaderos_a9, \
        p_criaderos_a10, p_criaderos_b1, p_criaderos_b2, \
        p_criaderos_b3, p_criaderos_b4, p_criaderos_b5, p_criaderos_b6, \
        p_criaderos_c1, p_criaderos_c2,\
        p_criaderos_c3, p_criaderos_c4, p_sw_criadero_a, p_sw_criadero_b, \
        p_sw_criadero_c))

@mod_movil.route('/tablas1')
def tabla_s1():
    tablas_s1 = controllers_movil.listar_tabla_s1()
    return jsonify(tablas_s1)
    
@mod_movil.route('/tablas1/<id_s1>')
def buscar_tabla_s1(id_s1):
    tablas_s1 = controllers_movil.buscar_tabla_s1(id_s1)
    return jsonify(tablas_s1)
    
@mod_movil.route('/tablas1/modificar', methods=['POST'])
def modificar_tabla_s1():
    if request.method == 'POST':
        data = request.json
        p_id = data['id']
        p_fk_tipo_control = data['fk_tipo_control']
        p_fk_barrio = data['fk_barrio']
        p_fk_encargado = data['fk_encargado']
        p_fk_supervisor = data['fk_supervisor']
        p_fk_operador = data['fk_operador']
        p_cuadrilla = data['cuadrilla']
        p_brigada = data['brigada']
        p_fk_usuario = data['fk_usuario']

        if not controllers_movil.verificar_datos_requeridos_tabla_s1(\
        p_fk_tipo_control, p_fk_barrio, p_fk_encargado, p_fk_supervisor, \
        p_fk_operador, p_cuadrilla, p_brigada, p_fk_usuario):
            return jsonify(respuesta = "Datos requeridos no ingresados")

        return jsonify(respuesta = controllers_movil.modificar_tabla_s1(\
        p_id, p_fk_tipo_control, p_fk_barrio, p_fk_encargado, p_fk_supervisor, \
        p_fk_operador, p_cuadrilla, p_brigada, p_fk_usuario))
    
@mod_movil.route('/registro/modificar', methods=['POST'])
def modificar_registro():
    
    if request.method == 'POST':
        data = request.json
        p_id = data['id']
        p_fk_tabla_s1 = data['fk_tabla_s1']
        p_manzana = data['manzana']
        p_direccion = data['direccion']
        p_verificado = data['verificado']
        p_fk_tipo_predio = data['fk_tipo_predio']
        p_jefe_familia = data['jefe_familia']
        p_habitantes = data['habitantes']
        p_febriles = data['febriles']
        p_sin_criaderos = data['sin_criaderos']
        p_criaderos_eliminados = data['criaderos_eliminados']
        p_tratados_fisicamente = data['tratados_fisicamente']
        p_tratados_larvicida = data['tratados_larvicida']
        p_larvicida_a = data['larvicida_a']
        #p_larvicida_b = data['larvicida_b']
        #p_larvicida_c = data['larvicida_c']
        p_criaderos_a1 = data['criaderos_a1']
        p_criaderos_a2 = data['criaderos_a2']
        p_criaderos_a3 = data['criaderos_a3']
        p_criaderos_a4 = data['criaderos_a4']
        p_criaderos_a5 = data['criaderos_a5']
        p_criaderos_a6 = data['criaderos_a6']
        p_criaderos_a7 = data['criaderos_a7']
        p_criaderos_a8 = data['criaderos_a8']
        p_criaderos_a9 = data['criaderos_a9']
        p_criaderos_a10 = data['criaderos_a10']
        p_criaderos_b1 = data['criaderos_b1']
        p_criaderos_b2 = data['criaderos_b2']
        p_criaderos_b3 = data['criaderos_b3']
        p_criaderos_b4 = data['criaderos_b4']
        p_criaderos_b5 = data['criaderos_b5']
        p_criaderos_b6 = data['criaderos_b6']
        p_criaderos_c1 = data['criaderos_c1']
        p_criaderos_c2 = data['criaderos_c2']
        p_criaderos_c3 = data['criaderos_c3']
        p_criaderos_c4 = data['criaderos_c4']
        
        if p_habitantes == '':
            p_habitantes = None
        if p_febriles == '':
            p_febriles = None

        if not controllers_movil.verificar_datos_requeridos_registro( \
        p_manzana, p_direccion, p_verificado, p_fk_tipo_predio):
            return jsonify(respuesta = "Datos requeridos no ingresados")

        return jsonify(respuesta = controllers_movil.modificar_registro( \
        p_id, p_fk_tabla_s1, p_manzana, p_direccion, p_verificado, \
        p_fk_tipo_predio, p_jefe_familia, p_habitantes,\
        p_febriles, p_sin_criaderos, p_criaderos_eliminados, \
        p_tratados_fisicamente, p_tratados_larvicida, p_larvicida_a, \
        #p_larvicida_b, p_larvicida_c, 
        p_criaderos_a1, \
        p_criaderos_a2, p_criaderos_a3, p_criaderos_a4, p_criaderos_a5, \
        p_criaderos_a6, p_criaderos_a7, p_criaderos_a8, p_criaderos_a9, \
        p_criaderos_a10, p_criaderos_b1, p_criaderos_b2, \
        p_criaderos_b3, p_criaderos_b4, p_criaderos_b5, p_criaderos_b6, \
        p_criaderos_c1, p_criaderos_c2, p_criaderos_c3, p_criaderos_c4))

@mod_movil.route('/contrasena', methods=['GET','POST'])
def modificar_contrasena():
    if request.method == 'POST':
        data = request.json
        p_passanterior = data['passanterior']
        p_passnueva = data['passnueva']
        p_passnueva2 = data['passnueva2']
        p_id_usuario = data['id_usuario']

        if not controllers_movil.verificar_datos_requeridos_contrasena\
        (p_passanterior, p_passnueva, p_passnueva2):
            return jsonify(respuesta = "Datos requeridos no ingresados")

        return jsonify(respuesta = controllers_movil.modificar_contrasena(p_id_usuario, \
        p_passanterior, p_passnueva, p_passnueva2))
