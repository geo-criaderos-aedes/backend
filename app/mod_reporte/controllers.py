from database import db_session
from app.mod_user.models import Usuario
from app.mod_tbls1.models import TipoPredio, TipoControl, TablaS1, \
RegistroS1, CriaderoA, CriaderoB, CriaderoC, Departamento, Distrito, Barrio
from reportlab.graphics.shapes import Drawing
from reportlab.graphics.charts.linecharts import HorizontalLineChart
from reportlab.platypus import *
from reportlab.lib.styles import getSampleStyleSheet, TA_CENTER
from reportlab.rl_config import defaultPageSize
from reportlab.lib.units import inch
from reportlab.graphics.widgets.markers import makeMarker
from reportlab.lib import colors
from reportlab.pdfgen import canvas
from flask import make_response, Flask, send_file
from sqlalchemy import func
from sqlalchemy import or_
from sqlalchemy import and_

import sys
import string
import datetime
from cStringIO import StringIO

PAGE_HEIGHT=defaultPageSize[1]
styles = getSampleStyleSheet()
Institucion = "MINISTERIO DE SALUD PUBLICA Y BIENESTAR SOCIAL"
Dependencia = "SERVICIO NACIONAL DE ERRADICACION"
Dependencia2 = "DEL PALUDISMO"
Informe = "RESUMEN DIARIO DE RASTRILLAJE /MINGA/ BLOQUEO"
Elements=[]
HeaderStyle = styles["Heading4"]
HeaderStyle.alignment=TA_CENTER
ParaStyle = styles["Normal"]
PreStyle = styles["Code"]
 
def header(txt, style=HeaderStyle, klass=Paragraph, sep=0.3):
    s = Spacer(0.2*inch, sep*inch)
    para = klass(txt, style)
    sect = [s, para]
    result = KeepTogether(sect)
    return result
 
def p(txt):
    return header(txt, style=ParaStyle, sep=0.1)
 
def pre(txt):
    s = Spacer(0.1*inch, 0.1*inch)
    p = Preformatted(txt, PreStyle)
    precomps = [s,p]
    result = KeepTogether(precomps)
    return result
 
def obtener_suma_criaderos( p_barrio, p_tipo_control, p_fecha ):
    #p_fecha = datetime.datetime.now().strftime("%Y-%m-%d")
    #p_fecha = datetime.datetime(2017,5,7).strftime("%Y-%m-%d")
    #p_tipo_control = 1
    #p_barrio = 1

    tabla_criadero = db_session.query( TablaS1.id.label('tablas1_id'), RegistroS1.id.label('registros1_id'),\
    CriaderoA.id.label('criaderoa_id'), CriaderoA.criaderos_a1, CriaderoA.criaderos_a2, CriaderoA.criaderos_a3,\
    CriaderoA.criaderos_a4, CriaderoA.criaderos_a5, CriaderoA.criaderos_a6, CriaderoA.criaderos_a7,\
    CriaderoA.criaderos_a8, CriaderoA.criaderos_a9, CriaderoA.criaderos_a10,\
    CriaderoB.id.label('criaderob_id'), CriaderoB.criaderos_b1, CriaderoB.criaderos_b2, CriaderoB.criaderos_b3,\
    CriaderoB.criaderos_b4, CriaderoB.criaderos_b5, CriaderoB.criaderos_b6,\
    CriaderoC.id.label('criaderoc_id'), CriaderoC.criaderos_c1, CriaderoC.criaderos_c2, CriaderoC.criaderos_c3,\
    CriaderoC.criaderos_c4).\
    join(RegistroS1, RegistroS1.fk_tabla_s1 == TablaS1.id ).join(CriaderoA, CriaderoA.fk_registro_s1 == RegistroS1.id ).\
    join(CriaderoB, CriaderoB.fk_registro_s1 == RegistroS1.id ).join(CriaderoC, CriaderoC.fk_registro_s1 == RegistroS1.id ).\
    filter(TablaS1.estado == 'ACTIVO', TablaS1.fecha == p_fecha, TablaS1.fk_tipo_control == p_tipo_control,\
    TablaS1.fk_barrio == p_barrio).all()
    
    sum_a1 = 0
    sum_a2 = 0
    sum_a3 = 0
    sum_a4 = 0
    sum_a5 = 0
    sum_a6 = 0
    sum_a7 = 0
    sum_a8 = 0
    sum_a9 = 0
    sum_a10 = 0
    
    sum_b1 = 0
    sum_b2 = 0
    sum_b3 = 0
    sum_b4 = 0
    sum_b5 = 0
    sum_b6 = 0
    
    sum_c1 = 0
    sum_c2 = 0
    sum_c3 = 0
    sum_c4 = 0
    total = 0
    
    for fila in tabla_criadero:
        if fila.criaderos_a1 != None:
            sum_a1 = sum_a1 + fila.criaderos_a1 
        if fila.criaderos_a2 != None:
            sum_a2 = sum_a2 + fila.criaderos_a2
        if fila.criaderos_a3 != None:
            sum_a3 = sum_a3 + fila.criaderos_a3
        if fila.criaderos_a4 != None:
            sum_a4 = sum_a4 + fila.criaderos_a4
        if fila.criaderos_a5 != None:
            sum_a5 = sum_a5 + fila.criaderos_a5
        if fila.criaderos_a6 != None:
            sum_a6 = sum_a6 + fila.criaderos_a6
        if fila.criaderos_a7 != None:
            sum_a7 = sum_a7 + fila.criaderos_a7
        if fila.criaderos_a8 != None:
            sum_a8 = sum_a8 + fila.criaderos_a8
        if fila.criaderos_a9 != None:
            sum_a9 = sum_a9 + fila.criaderos_a9
        if fila.criaderos_a10 != None:
            sum_a10 = sum_a10 + fila.criaderos_a10

        if fila.criaderos_b1 != None:
            sum_b1 = sum_b1 + fila.criaderos_b1
        if fila.criaderos_b2 != None:
            sum_b2 = sum_b2 + fila.criaderos_b2
        if fila.criaderos_b3 != None:
            sum_b3 = sum_b3 + fila.criaderos_b3
        if fila.criaderos_b4 != None:
            sum_b4 = sum_b4 + fila.criaderos_b4
        if fila.criaderos_b5 != None:
            sum_b5 = sum_b5 + fila.criaderos_b5
        if fila.criaderos_b6 != None:
            sum_b6 = sum_b6 + fila.criaderos_b6

        if fila.criaderos_c1 != None:
            sum_c1 = sum_c1 + fila.criaderos_c1
        if fila.criaderos_c2 != None:
            sum_c2 = sum_c2 + fila.criaderos_c2
        if fila.criaderos_c3 != None:
            sum_c3 = sum_c3 + fila.criaderos_c3
        if fila.criaderos_c4 != None:
            sum_c4 = sum_c4 + fila.criaderos_c4
        
        total = sum_a1 + sum_a2 + sum_a3 + sum_a4 + sum_a5 + sum_a6 + sum_a7 + sum_a8 + sum_a9 + sum_a10 + sum_b1 + sum_b2 + sum_b3 + sum_b4 + sum_b5 + sum_b6 + sum_c1 + sum_c2 + sum_c3 + sum_c4

    item = dict(sum_a1=sum_a1, sum_a2=sum_a2, sum_a3=sum_a3, sum_a4=sum_a4, sum_a5=sum_a5,\
                sum_a6=sum_a6, sum_a7=sum_a7, sum_a8=sum_a8, sum_a9=sum_a9, sum_a10=sum_a10,\
                sum_b1=sum_b1, sum_b2=sum_b2, sum_b3=sum_b3, sum_b4=sum_b4, sum_b5=sum_b5, sum_b6=sum_b6,\
                sum_c1=sum_c1, sum_c2=sum_c2, sum_c3=sum_c3, sum_c4=sum_c4, total=total)

    return item

def obtener_suma_predios( p_barrio, p_tipo_control, p_fecha ):
    #p_fecha = datetime.datetime(2017,5,7).strftime("%Y-%m-%d")
    #p_tipo_control = 1
    #p_barrio = 1
    
    q1 = db_session.query( TipoPredio.id, TipoPredio.nombre, func.count(RegistroS1.fk_tipo_predio).label('total') ).\
    outerjoin(RegistroS1, RegistroS1.fk_tipo_predio == TipoPredio.id ).outerjoin(TablaS1, TablaS1.id == RegistroS1.fk_tabla_s1 ).\
    filter( or_( and_( TablaS1.fecha == p_fecha, TablaS1.fk_tipo_control == p_tipo_control, TablaS1.fk_barrio == p_barrio ) ,\
    and_(TablaS1.fecha == None, TablaS1.fk_tipo_control == p_tipo_control, TablaS1.fk_barrio == p_barrio) ) ).group_by(TipoPredio.id)
    
    temp = db_session.query( TipoPredio.id ).\
    outerjoin(RegistroS1, RegistroS1.fk_tipo_predio == TipoPredio.id ).outerjoin(TablaS1, TablaS1.id == RegistroS1.fk_tabla_s1 ).\
    filter( or_( and_( TablaS1.fecha == p_fecha, TablaS1.fk_tipo_control == p_tipo_control, TablaS1.fk_barrio == p_barrio ) ,\
    and_(TablaS1.fecha == None, TablaS1.fk_tipo_control == p_tipo_control, TablaS1.fk_barrio == p_barrio) ) ).group_by(TipoPredio.id)
    
    q2 = db_session.query( TipoPredio.id, TipoPredio.nombre, func.sum(TipoPredio.id - TipoPredio.id).label('total') ).\
    outerjoin(RegistroS1, RegistroS1.fk_tipo_predio == TipoPredio.id ).outerjoin(TablaS1, TablaS1.id == RegistroS1.fk_tabla_s1 ).\
    filter( ~TipoPredio.id.in_( temp ) ).group_by(TipoPredio.id)
    
    result = q1.union(q2).all()
    
    total = 0
    items = []
    for fila in result:
        total = total + fila.total
        item = dict(nombre=fila.nombre, total=fila.total)
        items.append(item)

    item = dict(nombre='TOTAL', total=total)
    items.append(item)
    
    items = sorted(items, key=lambda elemento: elemento['total'])
        
    return items

def obtener_total_criaderos( p_barrio, p_tipo_control, p_fecha ):
    #p_fecha = datetime.datetime(2017,5,7).strftime("%Y-%m-%d")
    #p_tipo_control = 1
    #p_barrio = 1
    
    sum_sin_criaderos = db_session.query( RegistroS1.id ).join(TablaS1, TablaS1.id == RegistroS1.fk_tabla_s1 ).\
    filter( and_( TablaS1.estado == 'ACTIVO', TablaS1.fecha == p_fecha, TablaS1.fk_tipo_control == p_tipo_control,\
    TablaS1.fk_barrio == p_barrio, RegistroS1.sin_criaderos == True ) ).count()
    
    sum_con_criaderos = db_session.query( RegistroS1.id ).join(TablaS1, TablaS1.id == RegistroS1.fk_tabla_s1 ).\
    filter( and_( TablaS1.estado == 'ACTIVO', TablaS1.fecha == p_fecha, TablaS1.fk_tipo_control == p_tipo_control,\
    TablaS1.fk_barrio == p_barrio, RegistroS1.sin_criaderos == False ) ).count()
    
    cantidad_total = db_session.query( TablaS1.id.label('tablas1_id'), RegistroS1.id.label('registros1_id'),\
    RegistroS1.criaderos_eliminados, RegistroS1.tratados_fisicamente, RegistroS1.tratados_larvicida).\
    join(RegistroS1, RegistroS1.fk_tabla_s1 == TablaS1.id ).\
    filter( and_(TablaS1.estado == 'ACTIVO', TablaS1.fecha == p_fecha, TablaS1.fk_tipo_control == p_tipo_control,\
    TablaS1.fk_barrio == p_barrio ) ).all()
    
    sum_eliminados = 0
    sum_fisicamente = 0
    sum_larvicida = 0
    
    for fila in cantidad_total:
        if fila.criaderos_eliminados != None:
            sum_eliminados = sum_eliminados + fila.criaderos_eliminados 
        if fila.tratados_fisicamente != None:
            sum_fisicamente = sum_fisicamente + fila.tratados_fisicamente
        if fila.tratados_larvicida != None:
            sum_larvicida = sum_larvicida + fila.tratados_larvicida
            
    item = dict(sum_sin_criaderos=sum_sin_criaderos, sum_con_criaderos=sum_con_criaderos,\
                sum_eliminados=sum_eliminados, sum_fisicamente=sum_fisicamente, sum_larvicida=sum_larvicida)
    return item

def obtener_total_poblacion( p_barrio, p_tipo_control, p_fecha ):
    #p_fecha = datetime.datetime(2017,5,7).strftime("%Y-%m-%d")
    #p_tipo_control = 1
    #p_barrio = 1
    
    cantidad_total = db_session.query( TablaS1.id.label('tablas1_id'), RegistroS1.id.label('registros1_id'),\
    RegistroS1.habitantes, RegistroS1.febriles).\
    join(RegistroS1, RegistroS1.fk_tabla_s1 == TablaS1.id ).\
    filter( and_(TablaS1.estado == 'ACTIVO', TablaS1.fecha == p_fecha, TablaS1.fk_tipo_control == p_tipo_control,\
    TablaS1.fk_barrio == p_barrio ) ).all()
    
    sum_habitantes = 0
    sum_febriles = 0
    
    for fila in cantidad_total:
        if fila.habitantes != None:
            sum_habitantes = sum_habitantes + fila.habitantes 
        if fila.febriles != None:
            sum_febriles = sum_febriles + fila.febriles
            
    item = dict(sum_habitantes=sum_habitantes, sum_febriles=sum_febriles)
    return item

def obtener_tipo_control(id_tipo_control):
    tipo_control = db_session.query(TipoControl, TipoControl.nombre).filter_by(id=id_tipo_control).one()
    return tipo_control.nombre

def obtener_departamento_por_barrio(id_barrio):
    barrio = db_session.query(Barrio, Barrio.id, Barrio.fk_distrito).filter_by(id=id_barrio).one()
    
    distrito = db_session.query(Distrito, Distrito.id, Distrito.fk_departamento).filter_by(id=barrio.fk_distrito).one()
    
    departamento = db_session.query(Departamento, Departamento.id, Departamento.nombre).filter_by(id=distrito.fk_departamento).one()
    
    return departamento.nombre

def obtener_distrito_por_barrio(id_barrio):
    barrio = db_session.query(Barrio, Barrio.id, Barrio.fk_distrito).filter_by(id=id_barrio).one()
    
    distrito = db_session.query(Distrito, Distrito.id, Distrito.nombre).filter_by(id=barrio.fk_distrito).one()
    
    return distrito.nombre

def obtener_barrio(id_barrio):
    barrio = db_session.query(Barrio, Barrio.id, Barrio.nombre).filter_by(id=id_barrio).one()
    
    return barrio.nombre

def graficar_tablas( sum_criaderos, suma_predios, suma_total, suma_poblacion ):
    
    styleSheet = getSampleStyleSheet()
    
    data1= [['RECIPIENTES', 'EXISTENTES'],
           ['A. UTILES'],
           ['A1. Tanques/Cisternas', sum_criaderos['sum_a1'] ],
           ['A2. Tambor', sum_criaderos['sum_a2'] ],
           ['A3. Floreros con agua', sum_criaderos['sum_a3'] ],
           ['A4. Plantas en meceta con plato', sum_criaderos['sum_a4'] ],
           ['A5. Estanques/Fuentes ornamentales', sum_criaderos['sum_a5'] ],
           ['A6. Canaletas de Tejado', sum_criaderos['sum_a6'] ],
           ['A7. Bebederos de animales', sum_criaderos['sum_a7'] ],
           ['A8. Cantaros', sum_criaderos['sum_a8'] ],
           ['A9. Aljibes/Pozos', sum_criaderos['sum_a9'] ],
           ['A10. Otros utiles', sum_criaderos['sum_a10'] ],
           ['B. INSERVIBLES'],
           ['B1. Neumaticos usados', sum_criaderos['sum_b1'] ],
           ['B2. Aparatos domesticos descartables', sum_criaderos['sum_b2'] ],
           ['B3. Cubetas descatables', sum_criaderos['sum_b3'] ],
           ['B4. Latas', sum_criaderos['sum_b4'] ],
           ['B5. Botellas', sum_criaderos['sum_b5'] ],
           ['B6. Otros inservibles', sum_criaderos['sum_b6'] ],
           ['C. NATURALES'],
           ['C1. Agujeros de arboles', sum_criaderos['sum_c1'] ],
           ['C2. Axilas de hojas', sum_criaderos['sum_c2'] ],
           ['C3. Agujeros en piedra', sum_criaderos['sum_c3'] ],
           ['C4. Otros naturales', sum_criaderos['sum_c4'] ],
           ['TOTAL', sum_criaderos['total']],
          ]

    t1=Table(data1,style=[('BOX',(0,0),(-1,-1),2,colors.black),
                        ('GRID',(0,0),(-1,-1),0.5,colors.black)
    ], hAlign='LEFT')
    
    styleSheet = getSampleStyleSheet()
    
    data2 = []
    data2.append( ['PREDIOS'] )
    
    for dic in suma_predios:
        item = [ dic['nombre'], dic['total'] ]
        data2.append(item)

    t2=Table(data2,style=[('BOX',(0,0),(-1,-1),2,colors.black),
                        ('GRID',(0,0),(-1,-1),0.5,colors.black)
    ], hAlign='RIGHT')
    
    
    styleSheet = getSampleStyleSheet()
 
    data3= [['CRIADEROS'],
           ['Predios SIN criaderos', suma_total["sum_sin_criaderos"] ],
           ['Predios CON criaderos', suma_total["sum_con_criaderos"] ],
           ['CRIADEROS Eliminados', suma_total["sum_eliminados"] ],
           ['CRIADEROS tratados Fisicamente', suma_total["sum_fisicamente"] ],
           ['CRIADEROS tratados con Larvicida', suma_total["sum_larvicida"] ],
          ]

    t3=Table(data3,style=[('BOX',(0,0),(-1,-1),2,colors.black),
                        ('GRID',(0,0),(-1,-1),0.5,colors.black)
    ], hAlign='RIGHT' )
    
    
    styleSheet = getSampleStyleSheet()
    
    data4= [['POBLACION'],
           ['Total Habitantes', suma_poblacion["sum_habitantes"] ],
           ['Total Febriles', suma_poblacion["sum_febriles"] ],
          ]

    t4=Table(data4,style=[('BOX',(0,0),(-1,-1),2,colors.black),
                        ('GRID',(0,0),(-1,-1),0.5,colors.black)
    ], hAlign='LEFT' )
    
    t = [ [ [t1, t4], [t2, t3] ] ]
    temp = Table(t)
    
    return temp
 
def obtener_reporte_pdf( barrio, tipo_control, fecha ):

    pdf = StringIO()
    doc = SimpleDocTemplate(pdf)
    
    miInstitucion = header(Institucion)
    miDependencia = header(Dependencia, sep=0.01, style=HeaderStyle)
    miDependencia2 = header(Dependencia2, sep=0.01, style=HeaderStyle)
    miInforme = header(Informe, sep=0.1, style=HeaderStyle)
    head_info = [miInstitucion, miDependencia, miDependencia2, miInforme]
    Elements.extend(head_info)
    
    #barrio = 1
    #tipo_control = 1
    
    p_tipo_control = obtener_tipo_control( tipo_control )
    p_departamento = obtener_departamento_por_barrio( barrio )
    p_distrito = obtener_distrito_por_barrio( barrio )
    p_barrio = obtener_barrio( barrio )
    Labels = '<font size=8>TIPO DE CONTROL: %s </font>' % p_tipo_control + '<font size=8> FECHA: %s</font>' % fecha
    myLabels = p(Labels)
    Elements.append(myLabels)
    Labels = '<font size=8>DEPARTAMENTO: %s </font>' % p_departamento + '<font size=8> DISTRITO: %s </font>' \
    % p_distrito + '<font size=8> BARRIO: %s</font>' % p_barrio
    myLabels = p(Labels)
    Elements.append(myLabels)

    suma_recipientes = obtener_suma_criaderos( barrio, tipo_control, fecha )
    suma_predios = obtener_suma_predios( barrio, tipo_control, fecha )
    suma_criaderos = obtener_total_criaderos( barrio, tipo_control, fecha )
    suma_poblacion = obtener_total_poblacion( barrio, tipo_control, fecha )
    tablas = graficar_tablas( suma_recipientes, suma_predios, suma_criaderos, suma_poblacion)
    mis_tablas = [tablas]
    Elements.extend(mis_tablas)
    
    doc.build(Elements)
    pdf_file = pdf.getvalue()
    pdf.close()
    
    response = make_response(pdf_file)
    response.headers['Content-Disposition'] = "attachment; filename=Reporte_S3.pdf"
    response.mimetype = 'application/pdf'
    return response

def verificar_datos_requeridos(barrio, tipo_control, fecha):
    sw = True

    if barrio == "":
        sw = False
    elif tipo_control == "":
        sw = False
    elif fecha == "":
        sw = False
    return sw

def formato_fecha(fecha):
    try:
        _fecha = datetime.datetime.strptime(fecha, "%d/%m/%Y")
    except Exception as e:
        return e

    return 'OK'

def listar_tipo_control():
    tipo_control = db_session.query(TipoControl).all()

    items = []
    for fila_tipo_control in tipo_control:
        item = dict(id=fila_tipo_control.id, \
        nombre=fila_tipo_control.nombre)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def listar_distrito():
    items = []

    departamentos = db_session.query(Departamento).all()    
    for departamento in departamentos:
        distritos = db_session.query(Distrito).filter_by(\
        fk_departamento=departamento.id).all()
        for distrito in distritos:
            barrios = db_session.query(Barrio).filter_by(\
            fk_distrito=distrito.id).all()
            for barrio in barrios:
                _nombre = departamento.nombre + "-" + distrito.nombre \
                + "-" + barrio.nombre

                item = dict(id=barrio.id, nombre=_nombre)
                items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items
