# Import flask dependencies
from flask import Blueprint
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from functools import wraps

from app import login_required
from app.mod_auth import controllers as ctrl_auth
from app.mod_reporte import controllers as controllers_reporte

from datetime import datetime

def _get_unauthorized_view():
    return redirect(url_for('no_autorizado'))

def roles_required(*roles):
    """Decorador que especifica que un usuario debe tener todos los 
    roles especificados."""
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            id_usuario = session['id_usuario']
            continuar = ctrl_auth.roles_required(id_usuario, roles)
            if(continuar):
                return fn(*args, **kwargs)
            return _get_unauthorized_view()
        return decorated_view
    return wrapper

def roles_accepted(*roles):
    """Decorador que especifica que un usuario debe tener al menos uno
    de los roles especificados"""
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            id_usuario = session['id_usuario']
            continuar = ctrl_auth.roles_accepted(id_usuario, roles)
            if(continuar):
                return fn(*args, **kwargs)
            return _get_unauthorized_view()
        return decorated_view
    return wrapper

# Define the blueprint
mod_reporte = Blueprint('reporte', __name__, url_prefix='/reporte')

# Set the route and accepted methods
@mod_reporte.route('/reporte_s3', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMREP')
def reporte_s3():
    error = None
    id_usuario = session['id_usuario']

    lista_tipocontrol = controllers_reporte.listar_tipo_control()
    lista_barrio = controllers_reporte.listar_distrito()

    if request.method == 'POST':
        if request.form["action"] == "Salir":
            return redirect(url_for('auth.menu_principal'))

        if request.form["action"] == "Generar":
            s_fecha = request.form['fecha_rep']
            p_tipo_control = request.form['select_tipocontrol']
            p_barrio = request.form['select_barrio']

            int_tipo_control = int(p_tipo_control)
            int_barrio = int(p_barrio)

            if controllers_reporte.formato_fecha(s_fecha) != 'OK':
                error = "Formato de fecha no valido"
                return render_template('reporte/administrar_reportes.html', \
                id_usuario = id_usuario, error=error, \
                fecha_rep=s_fecha, tipo_control=int_tipo_control, \
                barrio=int_barrio, lista_tipocontrol=lista_tipocontrol, \
                lista_barrio=lista_barrio)

            p_fecha = datetime.strptime(s_fecha, "%d/%m/%Y")

            if controllers_reporte.verificar_datos_requeridos(\
            p_barrio, p_tipo_control, s_fecha):
                return controllers_reporte.obtener_reporte_pdf(p_barrio, \
                p_tipo_control, p_fecha)
            else:
                error = "Datos requeridos no ingresados"
                return render_template('reporte/administrar_reportes.html', \
                id_usuario = id_usuario, error=error, \
                fecha_rep=s_fecha, tipo_control=int_tipo_control, \
                barrio=int_barrio, lista_tipocontrol=lista_tipocontrol, \
                lista_barrio=lista_barrio)

    return render_template('reporte/administrar_reportes.html', \
    id_usuario = id_usuario, error=error, \
    lista_tipocontrol=lista_tipocontrol, \
    lista_barrio=lista_barrio)
