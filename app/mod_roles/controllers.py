from database import db_session

# Import module models
from app.mod_user.models import Rol, Permiso

from sqlalchemy import or_

from sqlalchemy.sql.expression import func

def listar_roles():
    roles = db_session.query(Rol).all()
    items = []
    for fila_rol in roles:
        item = dict(id=fila_rol.id, \
        nombre=fila_rol.nombre, descripcion=fila_rol.descripcion)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def obtener_rol(identificador):
    item = None
    contador = db_session.query(Rol).filter_by(id=identificador).count()

    if contador > 0:
        rol = db_session.query(Rol).filter_by(id=identificador).one()

        item = dict(nombre=rol.nombre, descripcion=rol.descripcion)

    return item

def buscar_roles(cadena, campos):
    items = []
    cadena_lwr = cadena.lower()

    if campos == "todos":
        roles = db_session.query(Rol).filter\
        (or_(func.lower(Rol.nombre).contains(cadena_lwr), \
        func.lower(Rol.descripcion).contains(cadena_lwr))).all()
    elif campos == "nombre":
        roles = db_session.query(Rol).filter\
        (func.lower(Rol.nombre).contains(cadena_lwr)).all()
    elif campos == "descripcion":
        roles = db_session.query(Rol).filter\
        (func.lower(Rol.descripcion).contains(cadena_lwr)).all()

    for fila_rol in roles:
        item = dict(id=fila_rol.id, \
        nombre=fila_rol.nombre, descripcion=fila_rol.descripcion)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def verificar_datos_requeridos_alta(nombre, descripcion):
    sw = True

    if nombre == "":
        sw = False
    elif descripcion == "":
        sw = False
    return sw

def insertar_rol(nombre, descripcion):
    contador = db_session.query(Rol). \
    filter_by(nombre=nombre).count()

    if contador > 0:
        return 'El rol ya existe'

    try:
        rol = Rol()
        rol.nombre = nombre
        rol.descripcion = descripcion
        db_session.add(rol)
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def validaciones_modificar_rol(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun rol
        error = "Debe seleccionar un rol"
    elif len(seleccionados) > 1: #Selecciono mas de un rol
        error = u"Debe seleccionar un \xfanico rol"

    return error

def verificar_datos_requeridos_modificacion(nombre, descripcion):
    sw = True

    if nombre == "":
        sw = False
    elif descripcion == "":
        sw = False
    return sw

def modificar_rol(identificador, nombre, descripcion):
    contador = db_session.query(Rol). \
    filter_by(nombre=nombre).count()

    if contador > 0:
        return 'El rol ya existe'

    try:
        #Rol a ser modificado
        rol = db_session.query(Rol).filter_by(id=\
        identificador).one()

        rol.nombre = nombre
        rol.descripcion = descripcion
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def validaciones_eliminar_rol(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun rol
        error = "Debe seleccionar al menos un rol"

    return error

def eliminar_rol(seleccionados):
    try:
        for elemento in seleccionados:
            rol = db_session.query(Rol).filter_by(id=elemento).one()
            db_session.delete(rol)
            db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message
    return 'OK'

def listar_permisos(identificador_rol):
    contador = db_session.query(Rol). \
    filter_by(id=identificador_rol).count()

    if contador == 0:
        return []

    rol = db_session.query(Rol).filter_by(id=identificador_rol).one()
    items = []
    for fila_permiso in rol.permisos:
        item = dict(id=fila_permiso.id, \
        nombre=fila_permiso.nombre, descripcion=fila_permiso.descripcion)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def buscar_permisos(cadena, campos, identificador_rol):
    items = []
    cadena_lwr = cadena.lower()

    if campos == "todos":
        permisos = db_session.query(Permiso).filter\
        (or_(func.lower(Permiso.nombre).contains(cadena_lwr), \
        func.lower(Permiso.descripcion).contains(cadena_lwr))).all()
    elif campos == "nombre":
        permisos = db_session.query(Permiso).filter\
        (func.lower(Permiso.nombre).contains(cadena_lwr)).all()
    elif campos == "descripcion":
        permisos = db_session.query(Permiso).filter\
        (func.lower(Permiso.descripcion).contains(cadena_lwr)).all()

    contador = db_session.query(Rol). \
    filter_by(id=identificador_rol).count()

    if contador == 0:
        return []

    rol = db_session.query(Rol).filter_by(id=identificador_rol).one()

    for fila_permiso in permisos:
        for permiso_rol in rol.permisos:
            if fila_permiso.id == permiso_rol.id:
                item = dict(id=fila_permiso.id, \
                nombre=fila_permiso.nombre, descripcion=\
                fila_permiso.descripcion)

                items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def obtener_permisos_roles_asignar(identificador):
    items = []

    contador = db_session.query(Rol).filter_by(id=\
    identificador).count()

    if contador == 0:
        return items

    rol = db_session.query(Rol).filter_by(id=\
    identificador).one()

    permisos = db_session.query(Permiso).all()

    for fila_permiso in permisos:
        continuar = True
        for permiso_rol in rol.permisos:
            if fila_permiso.id == permiso_rol.id:
                continuar = False
                break

        if continuar:
            item = dict(id=fila_permiso.id, \
            nombre=fila_permiso.nombre, descripcion=\
            fila_permiso.descripcion)

            items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def buscar_permisos_roles_asignar(cadena, campos, identificador_rol):
    items = []
    cadena_lwr = cadena.lower()

    if campos == "todos":
        permisos = db_session.query(Permiso).filter\
        (or_(func.lower(Permiso.nombre).contains(cadena_lwr), \
        func.lower(Permiso.descripcion).contains(cadena_lwr))).all()
    elif campos == "nombre":
        permisos = db_session.query(Permiso).filter\
        (func.lower(Permiso.nombre).contains(cadena_lwr)).all()
    elif campos == "descripcion":
        permisos = db_session.query(Permiso).filter\
        (func.lower(Permiso.descripcion).contains(cadena_lwr)).all()

    contador = db_session.query(Rol). \
    filter_by(id=identificador_rol).count()

    if contador == 0:
        return []

    rol = db_session.query(Rol). \
    filter_by(id=identificador_rol).one()

    for fila_permiso in permisos:
        continuar = True
        for permiso_rol in rol.permisos:
            if fila_permiso.id == permiso_rol.id:
                continuar = False
                break

        if continuar:
            item = dict(id=fila_permiso.id, \
            nombre=fila_permiso.nombre, descripcion=\
            fila_permiso.descripcion)

            items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def validaciones_asignar_permisos_roles(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun permiso
        error = "Debe seleccionar al menos un permiso"

    return error

def asignar_permisos_roles(seleccionados, identificador_rol):
    try:
        rol = db_session.query(Rol).filter_by(id=\
        identificador_rol).one()
        for elemento in seleccionados:
            permiso = db_session.query(Permiso).filter_by(id=\
            elemento).one()
            rol.permisos.append(permiso)
            db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message
    return 'OK'

def validaciones_quitar_permisos_rol(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun permiso
        error = "Debe seleccionar al menos un permiso"

    return error

def quitar_permisos_rol(seleccionados, identificador_rol):
    try:
        rol = db_session.query(Rol).filter_by(id=\
        identificador_rol).one()
        for elemento in seleccionados:
            permiso = db_session.query(Permiso).filter_by(id=\
            elemento).one()
            rol.permisos.remove(permiso)
            db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message
    return 'OK'
