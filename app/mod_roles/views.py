# Import flask dependencies
from flask import Blueprint
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from functools import wraps

from app import login_required
from app.mod_roles import controllers
from app.mod_auth import controllers as ctrl_auth

from config import REGISTERS_PER_PAGE
import utils

def _get_unauthorized_view():
    return redirect(url_for('no_autorizado'))

def roles_required(*roles):
    """Decorador que especifica que un usuario debe tener todos los 
    roles especificados."""
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            id_usuario = session['id_usuario']
            continuar = ctrl_auth.roles_required(id_usuario, roles)
            if(continuar):
                return fn(*args, **kwargs)
            return _get_unauthorized_view()
        return decorated_view
    return wrapper

def roles_accepted(*roles):
    """Decorador que especifica que un usuario debe tener al menos uno
    de los roles especificados"""
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            id_usuario = session['id_usuario']
            continuar = ctrl_auth.roles_accepted(id_usuario, roles)
            if(continuar):
                return fn(*args, **kwargs)
            return _get_unauthorized_view()
        return decorated_view
    return wrapper

# Define the blueprint
mod_roles = Blueprint('roles', __name__, url_prefix='/roles')

@mod_roles.route('/', methods=['GET', 'POST'])
@mod_roles.route('/pagina/<int:pagina>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMROL')
def administrar_roles(pagina=1):
    items = []
    error = None
    id_usuario = session['id_usuario']
    cadena = ""
    campo = "todos"
    if request.method == 'GET':
        busqueda = session['busqueda']
        if busqueda == True:
            cadena = session["cadena"]
            campo = session["campos"]
            items = controllers.buscar_roles(cadena, campo)
        else:
            items = controllers.listar_roles()
    elif request.method == 'POST':
        if request.form["action"] == "Buscar":
            cadena = request.form["cadena"]
            campo = request.form["campos"]
            session['cadena'] = cadena
            session['campos'] = campo
            items = controllers.buscar_roles(cadena, campo)
            if len(items) == 0:
                session['busqueda'] = False
                flash("No se encontraron coincidencias")
            else:
                session['busqueda'] = True
        elif request.form["action"] == "Limpiar":
            session['busqueda'] = False
            pagina = 1
            items = controllers.buscar_roles()
        elif request.form["action"] == "Salir":
            session['busqueda'] = False
            return redirect(url_for('auth.menu_principal'))
        elif request.form["action"] == "Crear":
            return redirect(url_for('roles.crear_roles'))
        elif request.form["action"] == "Modificar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_modificar_rol(seleccionados)
            if error == None: #Si paso las validaciones
                return redirect(url_for('roles.modificar_roles', rolmodif=seleccionados[0]))
        elif request.form["action"] == "Eliminar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_eliminar_rol(seleccionados)
            if error == None: #Si paso las validaciones
                resultado = controllers.eliminar_rol(seleccionados)
                if resultado == 'OK':
                    flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
                    items = controllers.listar_roles()
                else:
                    error = resultado

    paginacion_siguiente = utils.paginacion_siguiente(items, \
    pagina, REGISTERS_PER_PAGE)
    paginacion_anterior = utils.paginacion_anterior(items, pagina)
    items = utils.paginacion(items, pagina, REGISTERS_PER_PAGE)

    return render_template('roles/administrar_roles.html', items=items, \
    id_usuario=id_usuario, error=error, cadena=cadena, set_etc=campo,\
    pg_sgte=paginacion_siguiente, pg_ante=paginacion_anterior)

@mod_roles.route('/alta', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMROL')
def crear_roles():
    id_usuario = session['id_usuario']
    error = None
    if request.method == 'POST':
        if request.form["action"] == "Salir":
            return redirect(url_for('roles.administrar_roles'))

        p_nombre = request.form['nombre']
        p_descripcion = request.form['descripcion']

        if not controllers.verificar_datos_requeridos_alta(p_nombre, \
        p_descripcion):
            error = "Datos requeridos no ingresados"
            return render_template('roles/crear_roles.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre, \
            descripcion=p_descripcion)

        resultado = controllers.insertar_rol(p_nombre, \
        p_descripcion)

        if resultado == 'OK':
            flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
            if request.form["action"] == "Guardar y Continuar":
                return render_template('roles/crear_roles.html', \
                id_usuario=id_usuario, error=error)
            else: #Eligio Guardar
                return redirect(url_for('roles.administrar_roles'))
        else:
            error = resultado
            return render_template('roles/crear_roles.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre, \
            descripcion=p_descripcion)

    return render_template('roles/crear_roles.html', id_usuario = \
    id_usuario, error=error, nombre="", descripcion="")

@mod_roles.route('/modificacion/<rolmodif>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMROL')
def modificar_roles(rolmodif):
    id_usuario = session['id_usuario']
    error = None
    if request.method == 'POST':
        if request.form["action"] == "Salir":
            return redirect(url_for('roles.administrar_roles'))

        p_nombre = request.form['nombre']
        p_descripcion = request.form['descripcion']

        if not controllers.verificar_datos_requeridos_modificacion(\
        p_nombre, p_descripcion):
            error = "Datos requeridos no ingresados"
            return render_template('roles/modificar_roles.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre, \
            descripcion=p_descripcion)

        resultado = controllers.modificar_rol(rolmodif, \
        p_nombre, p_descripcion)

        if resultado == 'OK':
            flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
            return redirect(url_for('roles.administrar_roles'))
        else:
            error = resultado
            return render_template('roles/modificar_roles.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre, \
            descripcion=p_descripcion)

    rol = controllers.obtener_rol(rolmodif)

    if rol == None:
        error = "El rol no existe"
        return render_template('roles/modificar_roles.html', \
        id_usuario=id_usuario, error=error, nombre="", descripcion="")

    return render_template('roles/modificar_roles.html', \
    id_usuario = id_usuario, error=error, nombre=rol.get('nombre'), \
    descripcion=rol.get('descripcion'))

@mod_roles.route('/permisos/<rol>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMROL')
def administrar_permisos(rol):
    items = []
    error = None
    id_usuario = session['id_usuario']
    cadena = ""
    campo = "todos"
    if request.method == 'GET':
        busqueda = session['busqueda']
        if busqueda == True:
            cadena = session["cadena"]
            campo = session["campos"]
            items = controllers.buscar_permisos(cadena, campo)
        else:
            items = controllers.listar_permisos(rol)
    elif request.method == 'POST':
        if request.form["action"] == "Buscar":
            cadena = request.form["cadena"]
            campo = request.form["campos"]
            session['cadena'] = cadena
            session['campos'] = campo
            items = controllers.buscar_permisos(cadena, campo, rol)
            if len(items) == 0:
                session['busqueda'] = False
                flash("No se encontraron coincidencias")
            else:
                session['busqueda'] = True
        elif request.form["action"] == "Limpiar":
            session['busqueda'] = False
            items = controllers.listar_permisos(rol)
        elif request.form["action"] == "Salir":
            session['busqueda'] = False
            return redirect(url_for('roles.administrar_roles'))
        elif request.form["action"] == "Asignar":
            return redirect(url_for('roles.asignar_permisos_rol', rol=rol))
        elif request.form["action"] == "Quitar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_quitar_permisos_rol(seleccionados)
            if error == None: #Si paso las validaciones
                resultado = controllers.quitar_permisos_rol(seleccionados, \
                rol)
                if resultado == 'OK':
                    flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
                    items = controllers.listar_permisos(rol)
                else:
                    error = resultado

    return render_template('roles/administrar_permisos.html', items=items, \
    id_usuario=id_usuario, error=error, cadena=cadena, set_etc=campo)

@mod_roles.route('/permisos/asignar/<rol>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMROL')
def asignar_permisos_rol(rol):
    id_usuario = session['id_usuario']
    items = []
    error = None
    cadena = ""
    campo = "todos"
    if request.method == 'GET':
        busqueda = session['busqueda']
        if busqueda == True:
            cadena = session["cadena"]
            campo = session["campos"]
            items = controllers.buscar_permisos_roles_asignar(cadena, campo, rol)
        else:
            items = controllers.obtener_permisos_roles_asignar(rol)
    elif request.method == 'POST':
        if request.form["action"] == "Salir":
            session['busqueda'] = False
            return redirect(url_for('roles.administrar_permisos', \
            rol=rol))
        elif request.form["action"] == "Buscar":
            cadena = request.form["cadena"]
            campo = request.form["campos"]
            session['cadena'] = cadena
            session['campos'] = campo
            items = controllers.buscar_permisos_roles_asignar(cadena, campo, rol)
            if len(items) == 0:
                session['busqueda'] = False
                flash("No se encontraron coincidencias")
            else:
                session['busqueda'] = True
        elif request.form["action"] == "Limpiar":
            session['busqueda'] = False
            items = controllers.obtener_permisos_roles_asignar(rol)
        elif request.form["action"] == "Asignar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_asignar_permisos_roles(seleccionados)
            if error == None: #Si paso las validaciones
                resultado = controllers.asignar_permisos_roles(seleccionados, \
                rol)
                if resultado == 'OK':
                    flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
                    return redirect(url_for('roles.administrar_permisos', \
                    rol=rol))
                else:
                    error = resultado

    return render_template('roles/asignar_permisos_roles.html', \
    items=items, id_usuario=id_usuario, error=error, cadena=cadena, \
    set_etc=campo)
