from database import db_session

from sqlalchemy import String

# Import module models
from app.mod_tbls1.models import TablaS1, RegistroS1
from app.mod_tbls1.models import Departamento, Distrito, Barrio
from app.mod_tbls1.models import TipoControl, TipoPredio
from app.mod_tbls1.models import CriaderoA, CriaderoB, CriaderoC

from sqlalchemy import exc
from sqlalchemy import or_

from sqlalchemy.sql import text
from sqlalchemy.sql.expression import func

import utils

def listar_departamentos():
    items = []

    departamentos = db_session.query(Departamento).all()

    for fila_departamento in departamentos:
        item = dict(id=fila_departamento.id, \
        nombre=fila_departamento.nombre)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def buscar_departamentos(cadena):
    items = []
    cadena_lwr = cadena.lower()

    departamentos = db_session.query(Departamento).filter\
    (func.lower(Departamento.nombre).contains(cadena_lwr)).all()

    for fila_departamento in departamentos:
        item = dict(id=fila_departamento.id, \
        nombre=fila_departamento.nombre)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def verificar_datos_requeridos_alta_dpto(nombre):
    sw = True

    if nombre == "":
        sw = False
    return sw

def insertar_departamento(nombre):
    try:
        departamento = Departamento()
        departamento.nombre = nombre
        db_session.add(departamento)
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def validaciones_eliminar_departamento(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun departamento
        error = "Debe seleccionar al menos un departamento"

    return error

def eliminar_departamento(seleccionados):
    try:
        for elemento in seleccionados:
            departamento = db_session.query(Departamento).filter_by(id=\
            elemento).one()
            
            contador = db_session.query(Distrito)\
            .filter_by(fk_departamento=elemento).count()
            
            if contador > 0:
                return 'No se puede eliminar el departamento ' + departamento.nombre
            
            db_session.delete(departamento)
            db_session.flush()
            db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message
    return 'OK'

def validaciones_modificar_departamento(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun departamento
        error = "Debe seleccionar un departamento"
    elif len(seleccionados) > 1: #Selecciono mas de un departamento
        error = u"Debe seleccionar un \xfanico departamento"

    return error

def obtener_departamento(identificador):
    item = None
    contador = db_session.query(Departamento).filter_by(id=identificador).count()

    if contador > 0:
        dep = db_session.query(Departamento).filter_by(id=identificador).one()

        item = dict(nombre=dep.nombre)

    return item

def verificar_datos_requeridos_modif_dpto(nombre):
    sw = True

    if nombre == "":
        sw = False
    return sw

def modificar_departamento(identificador, nombre):
    try:
        #Departamento a ser modificado
        departamento = db_session.query(Departamento).filter_by(id=\
        identificador).one()

        departamento.nombre = nombre
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def listar_distritos(departamento):
    if departamento != '0':
        distritos = db_session.query(Distrito).\
        filter_by(fk_departamento=departamento).all()
    else:
        distritos = db_session.query(Distrito).all()

    items = []
    for fila_distrito in distritos:
        item = dict(id=fila_distrito.id, \
        nombre=fila_distrito.nombre)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def buscar_distritos(cadena, departamento):
    items = []
    cadena_lwr = cadena.lower()

    distritos = db_session.query(Distrito).filter\
    (func.lower(Distrito.nombre).contains(cadena_lwr),
    Distrito.fk_departamento==departamento).all()

    for fila_distrito in distritos:
        item = dict(id=fila_distrito.id, \
        nombre=fila_distrito.nombre)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def verificar_datos_requeridos_alta_dist(nombre):
    sw = True

    if nombre == "":
        sw = False
    return sw

def insertar_distrito(departamento, nombre):
    try:
        distrito = Distrito()
        distrito.fk_departamento = departamento
        distrito.nombre = nombre
        db_session.add(distrito)
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def validaciones_eliminar_distrito(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun distrito
        error = "Debe seleccionar al menos un distrito"

    return error

def eliminar_distrito(seleccionados):
    try:
        for elemento in seleccionados:
            distrito = db_session.query(Distrito).filter_by(id=\
            elemento).one()
            
            contador = db_session.query(Barrio)\
            .filter_by(fk_distrito=elemento).count()
            
            if contador > 0:
                return 'No se puede eliminar el distrito ' + distrito.nombre
            
            db_session.delete(distrito)
            db_session.flush()
            db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message
    return 'OK'

def validaciones_modificar_distrito(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun distrito
        error = "Debe seleccionar un distrito"
    elif len(seleccionados) > 1: #Selecciono mas de un distrito
        error = u"Debe seleccionar un \xfanico distrito"

    return error

def obtener_distrito(identificador):
    item = None
    contador = db_session.query(Distrito).filter_by(id=identificador).count()

    if contador > 0:
        dist = db_session.query(Distrito).filter_by(id=identificador).one()

        item = dict(departamento=dist.fk_departamento, nombre=dist.nombre)

    return item

def verificar_datos_requeridos_modif_dist(nombre):
    sw = True

    if nombre == "":
        sw = False
    return sw

def modificar_distrito(identificador, nombre):
    try:
        #Distrito a ser modificado
        distrito = db_session.query(Distrito).filter_by(id=\
        identificador).one()

        distrito.nombre = nombre
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def listar_barrios(distrito):
    if distrito != '0':
        barrios = db_session.query(Barrio).\
        filter_by(fk_distrito=distrito).all()
    else:
        barrios = db_session.query(Barrio).all()

    items = []
    for fila_barrio in barrios:
        item = dict(id=fila_barrio.id, \
        codigo=fila_barrio.codigo, \
        nombre=fila_barrio.nombre)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def buscar_barrios(cadena, campos, distrito):
    items = []
    cadena_lwr = cadena.lower()

    if campos == "todos":
        barrios = db_session.query(Barrio).filter\
        (or_(func.lower(Barrio.codigo).contains(cadena_lwr), \
        func.lower(Barrio.nombre).contains(cadena_lwr)),
        Barrio.fk_distrito==distrito).all()
    elif campos == "codigo":
        barrios = db_session.query(Barrio).filter\
        (func.lower(Barrio.codigo).contains(cadena_lwr),
        Barrio.fk_distrito==distrito).all()
    elif campos == "nombre":
        barrios = db_session.query(Barrio).filter\
        (func.lower(Barrio.nombre).contains(cadena_lwr),
        Barrio.fk_distrito==distrito).all()

    for fila_barrio in barrios:
        item = dict(id=fila_barrio.id, \
        codigo=fila_barrio.codigo, \
        nombre=fila_barrio.nombre)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def verificar_datos_requeridos_alta_barr(nombre):
    sw = True

    if nombre == "":
        sw = False
    return sw

def insertar_barrio(distrito, codigo, nombre):
    try:
        barrio = Barrio()
        barrio.fk_distrito = distrito
        barrio.codigo = codigo
        barrio.nombre = nombre
        db_session.add(barrio)
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def validaciones_eliminar_barrio(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun barrio
        error = "Debe seleccionar al menos un barrio"

    return error

def eliminar_barrio(seleccionados):
    try:
        for elemento in seleccionados:
            barrio = db_session.query(Barrio).filter_by(id=\
            elemento).one()
            
            contador = db_session.query(TablaS1)\
            .filter_by(fk_barrio=elemento).count()
            
            if contador > 0:
                return 'No se puede eliminar el barrio ' + barrio.nombre
            
            db_session.delete(barrio)
            db_session.flush()
            db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message
    return 'OK'

def validaciones_modificar_barrio(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun barrio
        error = "Debe seleccionar un barrio"
    elif len(seleccionados) > 1: #Selecciono mas de un barrio
        error = u"Debe seleccionar un \xfanico barrio"

    return error

def obtener_barrio(identificador):
    item = None
    contador = db_session.query(Barrio).filter_by(id=identificador).count()

    if contador > 0:
        barr = db_session.query(Barrio).filter_by(id=identificador).one()

        item = dict(distrito=barr.fk_distrito, codigo=barr.codigo, nombre=barr.nombre)

    return item

def verificar_datos_requeridos_modif_barr(nombre):
    sw = True

    if nombre == "":
        sw = False
    return sw

def modificar_barrio(identificador, codigo, nombre):
    try:
        #Barrio a ser modificado
        barrio = db_session.query(Barrio).filter_by(id=\
        identificador).one()

        barrio.codigo = codigo
        barrio.nombre = nombre
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def listar_planillas():
    items = []

    planillas = db_session.query(TablaS1).filter_by(estado='ACTIVO').all()

    for fila_planilla in planillas:
        barrio = fila_planilla.barrios
        distrito = barrio.distrito
        departamento = distrito.departamento
        usuario = fila_planilla.usuarios

        item = dict(id=fila_planilla.id, \
        barrio=barrio.nombre, distrito=distrito.nombre, \
        departamento=departamento.nombre, usuario=usuario.nombre + ' ' \
        + usuario.apellido, fecha=fila_planilla.fecha.strftime('%d/%m/%Y'))

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['id'], reverse=True)
    return items

def buscar_planillas(cadena, campos):
    items = []
    cadena_lwr = cadena.lower()

    if campos == "todos":
        cmd = 'select t.id id, d.nombre departamento, '\
        + 'dt.nombre distrito, b.nombre barrio, '\
        + '(u.nombre || \' \' || u.apellido) usuario, to_char(t.fecha, \'DD/MM/YYYY\') fecha ' \
        + 'from tabla_s1 t, departamento d, distrito dt, barrio b, ' \
        + 'usuario u ' \
        + 'where t.fk_barrio = b.id ' \
        + 'and b.fk_distrito = dt.id ' \
        + 'and dt.fk_departamento = d.id ' \
        + 'and u.id = t.fk_usuario ' \
        + 'and t.estado = \'ACTIVO\' ' \
        + 'and (lower(d.nombre) like :filtro ' \
        + 'or lower(dt.nombre) like :filtro ' \
        + 'or lower(b.nombre) like :filtro ' \
        + 'or lower(u.nombre || \' \' || u.apellido) like :filtro '\
        + 'or to_char(t.fecha, \'DD/MM/YYYY\') like :filtro)'
        cadena_lwr = '%' + cadena_lwr + '%'
    elif campos == "departamento":
        cmd = 'select t.id id, d.nombre departamento, '\
        + 'dt.nombre distrito, b.nombre barrio, '\
        + '(u.nombre || \' \' || u.apellido) usuario, to_char(t.fecha, \'DD/MM/YYYY\') fecha ' \
        + 'from tabla_s1 t, departamento d, distrito dt, barrio b, ' \
        + 'usuario u ' \
        + 'where t.fk_barrio = b.id ' \
        + 'and b.fk_distrito = dt.id ' \
        + 'and dt.fk_departamento = d.id ' \
        + 'and u.id = t.fk_usuario ' \
        + 'and t.estado = \'ACTIVO\' ' \
        + 'and lower(d.nombre) like :filtro'
        cadena_lwr = '%' + cadena_lwr + '%'
    elif campos == "distrito":
        cmd = 'select t.id id, d.nombre departamento, '\
        + 'dt.nombre distrito, b.nombre barrio, '\
        + '(u.nombre || \' \' || u.apellido) usuario, to_char(t.fecha, \'DD/MM/YYYY\') fecha ' \
        + 'from tabla_s1 t, departamento d, distrito dt, barrio b, ' \
        + 'usuario u ' \
        + 'where t.fk_barrio = b.id ' \
        + 'and b.fk_distrito = dt.id ' \
        + 'and dt.fk_departamento = d.id ' \
        + 'and u.id = t.fk_usuario ' \
        + 'and t.estado = \'ACTIVO\' ' \
        + 'and lower(dt.nombre) like :filtro'
        cadena_lwr = '%' + cadena_lwr + '%'
    elif campos == "barrio":
        cmd = 'select t.id id, d.nombre departamento, '\
        + 'dt.nombre distrito, b.nombre barrio, '\
        + '(u.nombre || \' \' || u.apellido) usuario, to_char(t.fecha, \'DD/MM/YYYY\') fecha ' \
        + 'from tabla_s1 t, departamento d, distrito dt, barrio b, ' \
        + 'usuario u ' \
        + 'where t.fk_barrio = b.id ' \
        + 'and b.fk_distrito = dt.id ' \
        + 'and dt.fk_departamento = d.id ' \
        + 'and u.id = t.fk_usuario ' \
        + 'and t.estado = \'ACTIVO\' ' \
        + 'and lower(b.nombre) like :filtro'
        cadena_lwr = '%' + cadena_lwr + '%'
    elif campos == "usuario":
        cmd = 'select t.id id, d.nombre departamento, '\
        + 'dt.nombre distrito, b.nombre barrio, '\
        + '(u.nombre || \' \' || u.apellido) usuario, to_char(t.fecha, \'DD/MM/YYYY\') fecha ' \
        + 'from tabla_s1 t, departamento d, distrito dt, barrio b, ' \
        + 'usuario u ' \
        + 'where t.fk_barrio = b.id ' \
        + 'and b.fk_distrito = dt.id ' \
        + 'and dt.fk_departamento = d.id ' \
        + 'and u.id = t.fk_usuario ' \
        + 'and t.estado = \'ACTIVO\' ' \
        + 'and lower(u.nombre || \' \' || u.apellido) like :filtro'
        cadena_lwr = '%' + cadena_lwr + '%'
    elif campos == "fecha":
        cmd = 'select t.id id, d.nombre departamento, '\
        + 'dt.nombre distrito, b.nombre barrio, '\
        + '(u.nombre || \' \' || u.apellido) usuario, to_char(t.fecha, \'DD/MM/YYYY\') fecha ' \
        + 'from tabla_s1 t, departamento d, distrito dt, barrio b, ' \
        + 'usuario u ' \
        + 'where t.fk_barrio = b.id ' \
        + 'and b.fk_distrito = dt.id ' \
        + 'and dt.fk_departamento = d.id ' \
        + 'and u.id = t.fk_usuario ' \
        + 'and t.estado = \'ACTIVO\' ' \
        + 'and t.fecha = :filtro'

    try:
        planillas = db_session.execute(text(cmd), {'filtro': cadena_lwr})
    except exc.SQLAlchemyError:
        planillas = [];

    for fila_planilla in planillas:
        item = dict(id=fila_planilla.id, barrio=fila_planilla.barrio, \
        distrito=fila_planilla.distrito, \
        departamento=fila_planilla.departamento, \
        usuario=fila_planilla.usuario, fecha=fila_planilla.fecha)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['id'], reverse=True)
    return items

def validaciones_detalles_planilla(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ninguna planilla
        error = "Debe seleccionar una planilla"
    elif len(seleccionados) > 1: #Selecciono mas de una planilla
        error = u"Debe seleccionar una \xfanica planilla"

    return error

def obtener_planilla(identificador):
    item = None
    contador = db_session.query(TablaS1).filter_by(id=identificador, \
    estado='ACTIVO').count()

    if contador > 0:
        planilla = db_session.query(TablaS1).filter_by(id=identificador, \
        estado='ACTIVO').one()

        barrio = planilla.barrios
        distrito = barrio.distrito
        departamento = distrito.departamento
        usuario = planilla.usuarios

        item = dict(id=planilla.id, \
        barrio=barrio.nombre, distrito=distrito.nombre, \
        departamento=departamento.nombre, usuario=usuario.nombre + ' ' \
        + usuario.apellido, fecha=planilla.fecha.strftime('%d/%m/%Y'))

    return item

def listar_registros(planilla):
    items = []

    registros = db_session.query(RegistroS1).\
    filter_by(fk_tabla_s1=planilla, estado='ACTIVO').all()

    for fila_registro in registros:
        item = dict(id=fila_registro.id, \
        manzana=fila_registro.manzana, direccion=fila_registro.direccion, \
        latitud=fila_registro.latitud, longitud=fila_registro.longitud)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['id'])
    return items

def buscar_registros(cadena, campos, planilla):
    items = []
    cadena_lwr = cadena.lower()

    if campos == "todos":
        registros = db_session.query(RegistroS1).filter\
        (or_(func.lower(RegistroS1.manzana).contains(cadena_lwr), \
        func.lower(RegistroS1.direccion).contains(cadena_lwr), \
        func.cast(RegistroS1.latitud, String).contains(cadena_lwr), \
        func.cast(RegistroS1.longitud, String).contains(cadena_lwr)), \
        RegistroS1.estado=='ACTIVO',
        RegistroS1.fk_tabla_s1==planilla).all()
    elif campos == "manzana":
        registros = db_session.query(RegistroS1).filter\
        (func.lower(RegistroS1.manzana).contains(cadena_lwr), \
        RegistroS1.estado=='ACTIVO',
        RegistroS1.fk_tabla_s1==planilla).all()
    elif campos == "direccion":
        registros = db_session.query(RegistroS1).filter\
        (func.lower(RegistroS1.direccion).contains(cadena_lwr), \
        RegistroS1.estado=='ACTIVO',
        RegistroS1.fk_tabla_s1==planilla).all()
    elif campos == "latitud":
        registros = db_session.query(RegistroS1).filter\
        (func.cast(RegistroS1.latitud, String).contains(cadena_lwr), \
        RegistroS1.estado=='ACTIVO',
        RegistroS1.fk_tabla_s1==planilla).all()
    elif campos == "longitud":
        registros = db_session.query(RegistroS1).filter\
        (func.cast(RegistroS1.longitud, String).contains(cadena_lwr), \
        RegistroS1.estado=='ACTIVO',
        RegistroS1.fk_tabla_s1==planilla).all()

    for fila_registro in registros:
        item = dict(id=fila_registro.id, \
        manzana=fila_registro.manzana, \
        direccion=fila_registro.direccion, \
        latitud=fila_registro.latitud, \
        longitud=fila_registro.longitud)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['id'])
    return items

def validaciones_detalles_registro(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun registro
        error = "Debe seleccionar un registro"
    elif len(seleccionados) > 1: #Selecciono mas de un registro
        error = u"Debe seleccionar un \xfanico registro"

    return error

def obtener_registro(identificador):
    item = None
    contador = db_session.query(RegistroS1).filter_by(id=\
    identificador, estado='ACTIVO').count()

    if contador > 0:
        registro = db_session.query(RegistroS1).filter_by(id=\
        identificador, estado='ACTIVO').one()

        item = dict(id=registro.id, tabla_s1=registro.fk_tabla_s1, \
        manzana=registro.manzana, direccion=registro.direccion, \
        latitud=registro.latitud, longitud=registro.longitud)

    return item

def listar_criaderos_a(identificador):
    item = None
    listaItems = []
    contador = db_session.query(CriaderoA).filter_by(\
    fk_registro_s1=identificador).count()

    if contador > 0:
        registro = db_session.query(CriaderoA).filter_by(\
        fk_registro_s1=identificador).one()

        item = dict(criaderos_a1=utils.nvl(registro.criaderos_a1, 0), criaderos_a2=utils.nvl(registro.criaderos_a2, 0), \
        criaderos_a3=utils.nvl(registro.criaderos_a3, 0), criaderos_a4=utils.nvl(registro.criaderos_a4, 0), \
        criaderos_a5=utils.nvl(registro.criaderos_a5, 0), criaderos_a6=utils.nvl(registro.criaderos_a6, 0), \
        criaderos_a7=utils.nvl(registro.criaderos_a7, 0), criaderos_a8=utils.nvl(registro.criaderos_a8, 0), \
        criaderos_a9=utils.nvl(registro.criaderos_a9, 0), criaderos_a10=utils.nvl(registro.criaderos_a10, 0))
    else:
        item = dict(criaderos_a1=0, criaderos_a2=0, \
        criaderos_a3=0, criaderos_a4=0, \
        criaderos_a5=0, criaderos_a6=0, \
        criaderos_a7=0, criaderos_a8=0, \
        criaderos_a9=0, criaderos_a10=0)

    listaItems.append(item)
    return listaItems

def listar_criaderos_b(identificador):
    item = None
    listaItems = []
    contador = db_session.query(CriaderoB).filter_by(\
    fk_registro_s1=identificador).count()

    if contador > 0:
        registro = db_session.query(CriaderoB).filter_by(\
        fk_registro_s1=identificador).one()

        item = dict(criaderos_b1=utils.nvl(registro.criaderos_b1, 0), criaderos_b2=utils.nvl(registro.criaderos_b2, 0), \
        criaderos_b3=utils.nvl(registro.criaderos_b3, 0), criaderos_b4=utils.nvl(registro.criaderos_b4, 0), \
        criaderos_b5=utils.nvl(registro.criaderos_b5, 0), criaderos_b6=utils.nvl(registro.criaderos_b6, 0))
    else:
        item = dict(criaderos_b1=0, criaderos_b2=0, \
        criaderos_b3=0, criaderos_b4=0, \
        criaderos_b5=0, criaderos_b6=0)

    listaItems.append(item)
    return listaItems

def listar_criaderos_c(identificador):
    item = None
    listaItems = []
    contador = db_session.query(CriaderoC).filter_by(\
    fk_registro_s1=identificador).count()

    if contador > 0:
        registro = db_session.query(CriaderoC).filter_by(\
        fk_registro_s1=identificador).one()

        item = dict(criaderos_c1=utils.nvl(registro.criaderos_c1, 0), criaderos_c2=utils.nvl(registro.criaderos_c2, 0), \
        criaderos_c3=utils.nvl(registro.criaderos_c3, 0), criaderos_c4=utils.nvl(registro.criaderos_c4, 0))
    else:
        item = dict(criaderos_c1=0, criaderos_c2=0, \
        criaderos_c3=0, criaderos_c4=0)

    listaItems.append(item)
    return listaItems

def listar_tipo_control():
    tipo_control = db_session.query(TipoControl).all()

    items = []
    for fila_tipo_control in tipo_control:
        item = dict(id=fila_tipo_control.id, \
        nombre=fila_tipo_control.nombre)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def buscar_tipo_control(cadena):
    items = []
    cadena_lwr = cadena.lower()

    tipo_control = db_session.query(TipoControl).filter\
    (func.lower(TipoControl.nombre).contains(cadena_lwr)).all()

    for fila_tipo_control in tipo_control:
        item = dict(id=fila_tipo_control.id, \
        nombre=fila_tipo_control.nombre)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def verificar_datos_requeridos_alta_tcont(nombre):
    sw = True

    if nombre == "":
        sw = False
    return sw

def insertar_tipo_control(nombre):
    try:
        tipo_control = TipoControl()
        tipo_control.nombre = nombre
        db_session.add(tipo_control)
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def validaciones_eliminar_tipo_control(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun tipo de control
        error = "Debe seleccionar al menos un tipo de control"

    return error

def eliminar_tipo_control(seleccionados):
    try:
        for elemento in seleccionados:
            tipo_control = db_session.query(TipoControl).filter_by(id=\
            elemento).one()
            
            contador = db_session.query(TablaS1)\
            .filter_by(fk_tipo_control=elemento).count()
            
            if contador > 0:
                return 'No se puede eliminar el tipo de control ' + tipo_control.nombre
            
            db_session.delete(tipo_control)
            db_session.flush()
            db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message
    return 'OK'

def validaciones_modificar_tipo_control(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun tipo de control
        error = "Debe seleccionar un tipo de control"
    elif len(seleccionados) > 1: #Selecciono mas de un tipo de control
        error = u"Debe seleccionar un \xfanico tipo de control"

    return error

def obtener_tipo_control(identificador):
    item = None
    contador = db_session.query(TipoControl).filter_by(id=identificador).count()

    if contador > 0:
        tcont = db_session.query(TipoControl).filter_by(id=identificador).one()

        item = dict(nombre=tcont.nombre)

    return item

def verificar_datos_requeridos_modif_tcont(nombre):
    sw = True

    if nombre == "":
        sw = False
    return sw

def modificar_tipo_control(identificador, nombre):
    try:
        #Tipo de Control a ser modificado
        tipo_control = db_session.query(TipoControl).filter_by(id=\
        identificador).one()

        tipo_control.nombre = nombre
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def listar_tipo_predio():
    tipo_predio = db_session.query(TipoPredio).all()

    items = []
    for fila_tipo_predio in tipo_predio:
        item = dict(id=fila_tipo_predio.id, \
        nombre=fila_tipo_predio.nombre)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def buscar_tipo_predio(cadena):
    items = []
    cadena_lwr = cadena.lower()

    tipo_predio = db_session.query(TipoPredio).filter\
    (func.lower(TipoPredio.nombre).contains(cadena_lwr)).all()

    for fila_tipo_predio in tipo_predio:
        item = dict(id=fila_tipo_predio.id, \
        nombre=fila_tipo_predio.nombre)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def verificar_datos_requeridos_alta_tpred(nombre):
    sw = True

    if nombre == "":
        sw = False
    return sw

def insertar_tipo_predio(nombre):
    try:
        tipo_predio = TipoPredio()
        tipo_predio.nombre = nombre
        db_session.add(tipo_predio)
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def validaciones_eliminar_tipo_predio(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun tipo de predio
        error = "Debe seleccionar al menos un tipo de predio"

    return error

def eliminar_tipo_predio(seleccionados):
    try:
        for elemento in seleccionados:
            tipo_predio = db_session.query(TipoPredio).filter_by(id=\
            elemento).one()
            
            contador = db_session.query(RegistroS1)\
            .filter_by(fk_tipo_predio=elemento).count()
            
            if contador > 0:
                return 'No se puede eliminar el tipo de predio ' + tipo_predio.nombre
            
            db_session.delete(tipo_predio)
            db_session.flush()
            db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message
    return 'OK'

def validaciones_modificar_tipo_predio(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun tipo de predio
        error = "Debe seleccionar un tipo de predio"
    elif len(seleccionados) > 1: #Selecciono mas de un tipo de predio
        error = u"Debe seleccionar un \xfanico tipo de predio"

    return error

def obtener_tipo_predio(identificador):
    item = None
    contador = db_session.query(TipoPredio).filter_by(id=identificador).count()

    if contador > 0:
        tcont = db_session.query(TipoPredio).filter_by(id=identificador).one()

        item = dict(nombre=tcont.nombre)

    return item

def verificar_datos_requeridos_modif_tpred(nombre):
    sw = True

    if nombre == "":
        sw = False
    return sw

def modificar_tipo_predio(identificador, nombre):
    try:
        #Tipo de Predio a ser modificado
        tipo_predio = db_session.query(TipoPredio).filter_by(id=\
        identificador).one()

        tipo_predio.nombre = nombre
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def validaciones_eliminar_registro(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun registro
        error = "Debe seleccionar al menos un registro"

    return error

def eliminar_registro(seleccionados):
    try:
        for elemento in seleccionados:
            registro = db_session.query(RegistroS1).filter_by(id=\
            elemento).one()
            registro.estado = 'INACTIVO'
            db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message
    return 'OK'

def validaciones_eliminar_planilla(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ninguna planilla
        error = "Debe seleccionar al menos una planilla"

    return error

def eliminar_planilla(seleccionados):
    try:
        for elemento in seleccionados:
            planilla = db_session.query(TablaS1).filter_by(id=\
            elemento).one()
            
            registros = db_session.query(RegistroS1).filter_by\
            (fk_tabla_s1=planilla.id).all()
            
            for registro in registros:
                registro.estado = 'INACTIVO'
                db_session.flush()
            
            planilla.estado = 'INACTIVO'
            db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message
    return 'OK'
