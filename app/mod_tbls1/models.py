from sqlalchemy import Boolean, DateTime, Integer, Numeric, String
from sqlalchemy import Column, ForeignKey
from database import Base

from sqlalchemy.orm import relationship

class TablaS1(Base):
    __tablename__ = 'tabla_s1'

    id = Column(Integer, primary_key=True)
    fk_tipo_control = Column(Integer, ForeignKey('tipo_control.id'))
    fk_barrio = Column(Integer, ForeignKey('barrio.id'))
    fk_encargado = Column(Integer, ForeignKey('usuario.id'))
    fk_supervisor = Column(Integer, ForeignKey('usuario.id'))
    fk_operador = Column(Integer, ForeignKey('usuario.id'))
    cuadrilla = Column(String(20), nullable=False)
    brigada = Column(String(20), nullable=False)
    fk_usuario = Column(Integer, ForeignKey('usuario.id'))
    fecha = Column(DateTime, nullable=False)
    estado = Column(String(10), nullable=False)
    tipos_control = relationship('TipoControl', backref='tabla_s1')
    barrios = relationship('Barrio', backref='tabla_s1_barrios')
    encargados = relationship('Usuario', foreign_keys=[fk_encargado], backref='tabla_s1_encargados')
    supervisores = relationship('Usuario', foreign_keys=[fk_supervisor], backref='tabla_s1_supervisores')
    operadores = relationship('Usuario', foreign_keys=[fk_operador], backref='tabla_s1_operadores')
    usuarios = relationship('Usuario', foreign_keys=[fk_usuario], backref='tabla_s1_usuarios')

class RegistroS1(Base):
    __tablename__ = 'registro_s1'

    id = Column(Integer, primary_key=True)
    fk_tabla_s1 = Column(Integer, ForeignKey('tabla_s1.id'))
    manzana = Column(String(20))
    direccion = Column(String(50))
    latitud = Column(Numeric)
    longitud = Column(Numeric)
    verificado = Column(Boolean)
    fk_tipo_predio = Column(Integer, ForeignKey('tipo_predio.id'))
    jefe_familia = Column(String(50))
    habitantes = Column(Integer)
    febriles = Column(Integer)
    sin_criaderos = Column(Boolean)
    criaderos_eliminados = Column(Integer)
    tratados_fisicamente = Column(Integer)
    tratados_larvicida = Column(Integer)
    larvicida_a = Column(Integer)
    larvicida_b = Column(Integer)
    larvicida_c = Column(Integer)
    fecha = Column(DateTime, nullable=False)
    estado = Column(String(10), nullable=False)
    tabla_s1 = relationship('TablaS1', backref='registro_s1')
    tipos_predio = relationship('TipoPredio', backref='registro_s1')

class CriaderoA(Base):
    __tablename__ = 'criadero_a'

    id = Column(Integer, primary_key=True)
    fk_registro_s1 = Column(Integer, ForeignKey('registro_s1.id'))
    criaderos_a1 = Column(Integer)
    criaderos_a2 = Column(Integer)
    criaderos_a3 = Column(Integer)
    criaderos_a4 = Column(Integer)
    criaderos_a5 = Column(Integer)
    criaderos_a6 = Column(Integer)
    criaderos_a7 = Column(Integer)
    criaderos_a8 = Column(Integer)
    criaderos_a9 = Column(Integer)
    criaderos_a10 = Column(Integer)
    registros_s1 = relationship('RegistroS1', backref='criadero_a')

class CriaderoB(Base):
    __tablename__ = 'criadero_b'

    id = Column(Integer, primary_key=True)
    fk_registro_s1 = Column(Integer, ForeignKey('registro_s1.id'))
    criaderos_b1 = Column(Integer)
    criaderos_b2 = Column(Integer)
    criaderos_b3 = Column(Integer)
    criaderos_b4 = Column(Integer)
    criaderos_b5 = Column(Integer)
    criaderos_b6 = Column(Integer)
    registros_s1 = relationship('RegistroS1', backref='criadero_b')

class CriaderoC(Base):
    __tablename__ = 'criadero_c'

    id = Column(Integer, primary_key=True)
    fk_registro_s1 = Column(Integer, ForeignKey('registro_s1.id'))
    criaderos_c1 = Column(Integer)
    criaderos_c2 = Column(Integer)
    criaderos_c3 = Column(Integer)
    criaderos_c4 = Column(Integer)
    registros_s1 = relationship('RegistroS1', backref='criadero_c')

class TipoPredio(Base):
    __tablename__ = 'tipo_predio'

    id = Column(Integer, primary_key=True)
    nombre = Column(String(20))

class TipoControl(Base):
    __tablename__ = 'tipo_control'

    id = Column(Integer, primary_key=True)
    nombre = Column(String(20))

class Departamento(Base):
    __tablename__ = 'departamento'

    id = Column(Integer, primary_key=True)
    nombre = Column(String(35), nullable=False)
    distritos = relationship('Distrito', backref='departamento')

class Distrito(Base):
    __tablename__ = 'distrito'

    id = Column(Integer, primary_key=True)
    fk_departamento = Column(Integer, ForeignKey('departamento.id'))
    nombre = Column(String(35), nullable=False)
    barrios = relationship('Barrio', backref='distrito')

class Barrio(Base):
    __tablename__ = 'barrio'

    id = Column(Integer, primary_key=True)
    fk_distrito = Column(Integer, ForeignKey('distrito.id'))
    codigo = Column(String(8))
    nombre = Column(String(35), nullable=False)
