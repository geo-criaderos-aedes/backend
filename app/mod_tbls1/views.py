# Import flask dependencies
from flask import Blueprint
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from functools import wraps

from app import login_required
from app.mod_tbls1 import controllers
from app.mod_auth import controllers as ctrl_auth

from config import REGISTERS_PER_PAGE
import utils

def _get_unauthorized_view():
    return redirect(url_for('no_autorizado'))

def roles_required(*roles):
    """Decorador que especifica que un usuario debe tener todos los 
    roles especificados."""
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            id_usuario = session['id_usuario']
            continuar = ctrl_auth.roles_required(id_usuario, roles)
            if(continuar):
                return fn(*args, **kwargs)
            return _get_unauthorized_view()
        return decorated_view
    return wrapper

def roles_accepted(*roles):
    """Decorador que especifica que un usuario debe tener al menos uno
    de los roles especificados"""
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            id_usuario = session['id_usuario']
            continuar = ctrl_auth.roles_accepted(id_usuario, roles)
            if(continuar):
                return fn(*args, **kwargs)
            return _get_unauthorized_view()
        return decorated_view
    return wrapper

# Define the blueprint
mod_tbls1 = Blueprint('tbls1', __name__, url_prefix='/tbls1')

# Set the route and accepted methods
@mod_tbls1.route('/administrar_departamentos', methods=['GET', 'POST'])
@mod_tbls1.route('/administrar_departamentos/pagina/<int:pagina>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMLOC')
def administrar_departamentos(pagina=1):
    items = []
    error = None
    id_usuario = session['id_usuario']
    cadena = ""
    if request.method == 'GET':
        busqueda = session['busqueda']
        if busqueda == True:
            cadena = session["cadena"]
            campo = session["campos"]
            items = controllers.buscar_departamentos(cadena)
        else:
            items = controllers.listar_departamentos()
    elif request.method == 'POST':
        if request.form["action"] == "Buscar":
            cadena = request.form["cadena"]
            session['cadena'] = cadena
            items = controllers.buscar_departamentos(cadena)
            if len(items) == 0:
                session['busqueda'] = False
                flash("No se encontraron coincidencias")
            else:
                session['busqueda'] = True
        elif request.form["action"] == "Limpiar":
            session['busqueda'] = False
            pagina = 1
            items = controllers.listar_departamentos()
        elif request.form["action"] == "Salir":
            session['busqueda'] = False
            return redirect(url_for('auth.menu_principal'))
        elif request.form["action"] == "Crear":
            return redirect(url_for('tbls1.crear_departamentos'))
        elif request.form["action"] == "Modificar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_modificar_departamento(seleccionados)
            if error == None: #Si paso las validaciones
                return redirect(url_for('tbls1.modificar_departamentos', depmodif=seleccionados[0]))
            else: #No paso las validaciones
                return render_template('tbls1/administrar_departamentos.html', \
                items=items, id_usuario=id_usuario, error=error, \
                cadena=cadena)
        elif request.form["action"] == "Eliminar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_eliminar_departamento(seleccionados)
            if error == None: #Si paso las validaciones
                resultado = controllers.eliminar_departamento(seleccionados)
                if resultado == 'OK':
                    flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
                    items = controllers.listar_departamentos()
                else:
                    error = resultado
                    items = controllers.listar_departamentos()

    paginacion_siguiente = utils.paginacion_siguiente(items, \
    pagina, REGISTERS_PER_PAGE)
    paginacion_anterior = utils.paginacion_anterior(items, pagina)
    paginacion_primero = utils.paginacion_primero(items, pagina)
    paginacion_ultimo = utils.paginacion_ultimo(items, pagina, \
    REGISTERS_PER_PAGE)

    items = utils.paginacion(items, pagina, REGISTERS_PER_PAGE)

    return render_template('tbls1/administrar_departamentos.html', items=items, \
    id_usuario=id_usuario, cadena=cadena, error=error, \
    pg_ante=paginacion_anterior, pg_sgte=paginacion_siguiente, \
    pg_prim=paginacion_primero, pg_ultm=paginacion_ultimo)

@mod_tbls1.route('/alta_departamentos', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMLOC')
def crear_departamentos():
    id_usuario = session['id_usuario']
    error = None
    if request.method == 'POST':
        if request.form["action"] == "Salir":
            return redirect(url_for('tbls1.administrar_departamentos'))

        p_nombre = request.form['nombre']

        if not controllers.verificar_datos_requeridos_alta_dpto(p_nombre):
            error = "Datos requeridos no ingresados"
            return render_template('tbls1/crear_departamentos.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre)

        resultado = controllers.insertar_departamento(p_nombre)

        if resultado == 'OK':
            flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
            if request.form["action"] == "Guardar y Continuar":
                return render_template('tbls1/crear_departamentos.html', \
                id_usuario=id_usuario, error=error)
            else: #Eligio Guardar
                return redirect(url_for('tbls1.administrar_departamentos'))
        else:
            error = resultado
            return render_template('tbls1/crear_departamentos.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre)

    return render_template('tbls1/crear_departamentos.html', id_usuario = \
    id_usuario, error=error, nombre="")

@mod_tbls1.route('/modificacion_departamentos/<depmodif>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMLOC')
def modificar_departamentos(depmodif):
    id_usuario = session['id_usuario']
    error = None
    if request.method == 'POST':
        if request.form["action"] == "Salir":
            return redirect(url_for('tbls1.administrar_departamentos'))

        p_nombre = request.form['nombre']

        if not controllers.verificar_datos_requeridos_modif_dpto(\
        p_nombre):
            error = "Datos requeridos no ingresados"
            return render_template('tbls1/modificar_departamentos.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre)

        resultado = controllers.modificar_departamento(depmodif, p_nombre)

        if resultado == 'OK':
            flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
            return redirect(url_for('tbls1.administrar_departamentos'))
        else:
            error = resultado
            return render_template('tbls1/modificar_departamentos.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre)

    dep = controllers.obtener_departamento(depmodif)

    if dep == None:
        error = "El departamento no existe"
        return render_template('tbls1/modificar_departamentos.html', \
        id_usuario=id_usuario, error=error, nombre="")

    return render_template('tbls1/modificar_departamentos.html', \
    id_usuario = id_usuario, error=error, nombre=dep.get('nombre'))

@mod_tbls1.route('/administrar_distritos/<departamento>', methods=['GET', 'POST'])
@mod_tbls1.route('/administrar_distritos/<departamento>/pagina/<int:pagina>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMLOC')
def administrar_distritos(departamento, pagina=1):
    items = []
    error = None
    id_usuario = session['id_usuario']
    cadena = ""
    campo = "todos"
    if request.method == 'GET':
        busqueda = session['busqueda']
        if busqueda == True:
            cadena = session["cadena"]
            campo = session["campos"]
            items = controllers.buscar_distritos(cadena, departamento)
        else:
            items = controllers.listar_distritos(departamento)
    elif request.method == 'POST':
        if request.form["action"] == "Buscar":
            cadena = request.form["cadena"]
            session['cadena'] = cadena
            items = controllers.buscar_distritos(cadena, departamento)
            if len(items) == 0:
                session['busqueda'] = False
                flash("No se encontraron coincidencias")
            else:
                session['busqueda'] = True
        elif request.form["action"] == "Limpiar":
            session['busqueda'] = False
            pagina = 1
            items = controllers.listar_distritos(departamento)
        elif request.form["action"] == "Salir":
            session['busqueda'] = False
            return redirect(url_for('tbls1.administrar_departamentos'))
        elif request.form["action"] == "Crear":
            return redirect(url_for('tbls1.crear_distritos', \
            departamento=departamento))
        elif request.form["action"] == "Modificar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_modificar_distrito(seleccionados)
            if error == None: #Si paso las validaciones
                return redirect(url_for('tbls1.modificar_distritos', distmodif=seleccionados[0]))
            else: #No paso las validaciones
                return render_template('tbls1/administrar_distritos.html', \
                items=items, id_usuario=id_usuario, error=error, \
                cadena=cadena)
        elif request.form["action"] == "Eliminar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_eliminar_distrito(seleccionados)
            if error == None: #Si paso las validaciones
                resultado = controllers.eliminar_distrito(seleccionados)
                if resultado == 'OK':
                    flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
                    items = controllers.listar_distritos(departamento)
                else:
                    error = resultado
                    items = controllers.listar_distritos(departamento)

    paginacion_siguiente = utils.paginacion_siguiente(items, \
    pagina, REGISTERS_PER_PAGE)
    paginacion_anterior = utils.paginacion_anterior(items, pagina)
    paginacion_primero = utils.paginacion_primero(items, pagina)
    paginacion_ultimo = utils.paginacion_ultimo(items, pagina, \
    REGISTERS_PER_PAGE)

    items = utils.paginacion(items, pagina, REGISTERS_PER_PAGE)

    return render_template('tbls1/administrar_distritos.html', items=items, \
    id_usuario=id_usuario, cadena=cadena, set_etc=campo, error=error, \
    pg_ante=paginacion_anterior, pg_sgte=paginacion_siguiente, \
    pg_prim=paginacion_primero, pg_ultm=paginacion_ultimo, dpto=departamento)

@mod_tbls1.route('/alta_distritos/<departamento>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMLOC')
def crear_distritos(departamento):
    id_usuario = session['id_usuario']
    error = None
    if request.method == 'POST':
        if request.form["action"] == "Salir":
            return redirect(url_for('tbls1.administrar_distritos', \
            departamento=departamento))

        p_nombre = request.form['nombre']

        if not controllers.verificar_datos_requeridos_alta_dist(p_nombre):
            error = "Datos requeridos no ingresados"
            return render_template('tbls1/crear_distritos.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre)

        resultado = controllers.insertar_distrito(departamento, p_nombre)

        if resultado == 'OK':
            flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
            if request.form["action"] == "Guardar y Continuar":
                return render_template('tbls1/crear_distritos.html', \
                id_usuario=id_usuario, error=error)
            else: #Eligio Guardar
                return redirect(url_for('tbls1.administrar_distritos', \
                departamento=departamento))
        else:
            error = resultado
            return render_template('tbls1/crear_distritos.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre)

    return render_template('tbls1/crear_distritos.html', id_usuario = \
    id_usuario, error=error, nombre="")

@mod_tbls1.route('/modificacion_distritos/<distmodif>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMLOC')
def modificar_distritos(distmodif):
    id_usuario = session['id_usuario']
    error = None
    if request.method == 'POST':
        if request.form["action"] == "Salir":
            dist = controllers.obtener_distrito(distmodif)
            departamento = dist.get('departamento')
            return redirect(url_for('tbls1.administrar_distritos', \
            departamento=departamento))

        p_nombre = request.form['nombre']

        if not controllers.verificar_datos_requeridos_modif_dist(\
        p_nombre):
            error = "Datos requeridos no ingresados"
            return render_template('tbls1/modificar_distritos.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre)

        resultado = controllers.modificar_distrito(distmodif, p_nombre)

        if resultado == 'OK':
            flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
            dist = controllers.obtener_distrito(distmodif)
            departamento = dist.get('departamento')
            return redirect(url_for('tbls1.administrar_distritos',\
            departamento=departamento))
        else:
            error = resultado
            return render_template('tbls1/modificar_distritos.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre)

    dist = controllers.obtener_distrito(distmodif)

    if dist == None:
        error = "El distrito no existe"
        return render_template('tbls1/modificar_distritos.html', \
        id_usuario=id_usuario, error=error, nombre="")

    return render_template('tbls1/modificar_distritos.html', \
    id_usuario = id_usuario, error=error, nombre=dist.get('nombre'))

@mod_tbls1.route('/administrar_barrios/<distrito>', methods=['GET', 'POST'])
@mod_tbls1.route('/administrar_barrios/<distrito>/pagina/<int:pagina>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMLOC')
def administrar_barrios(distrito, pagina=1):
    items = []
    error = None
    id_usuario = session['id_usuario']
    cadena = ""
    campo = "todos"
    items = controllers.listar_barrios(distrito)
    if request.method == 'GET':
        busqueda = session['busqueda']
        if busqueda == True:
            cadena = session["cadena"]
            campo = session["campos"]
            items = controllers.buscar_barrios(cadena, campo, distrito)
        else:
            items = controllers.listar_barrios(distrito)
    elif request.method == 'POST':
        if request.form["action"] == "Buscar":
            cadena = request.form["cadena"]
            campo = request.form["campos"]
            session['cadena'] = cadena
            session['campos'] = campo
            items = controllers.buscar_barrios(cadena, campo, distrito)
            if len(items) == 0:
                session['busqueda'] = False
                flash("No se encontraron coincidencias")
            else:
                session['busqueda'] = True
        elif request.form["action"] == "Limpiar":
            session['busqueda'] = False
            pagina = 1
            items = controllers.listar_barrios(distrito)
        elif request.form["action"] == "Salir":
            session['busqueda'] = False
            dist = controllers.obtener_distrito(distrito)
            departamento = dist.get('departamento')
            return redirect(url_for('tbls1.administrar_distritos', \
            departamento=departamento))
        elif request.form["action"] == "Crear":
            return redirect(url_for('tbls1.crear_barrios', \
            distrito=distrito))
        elif request.form["action"] == "Modificar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_modificar_barrio(seleccionados)
            if error == None: #Si paso las validaciones
                return redirect(url_for('tbls1.modificar_barrios', barrmodif=seleccionados[0]))
            else: #No paso las validaciones
                return render_template('tbls1/administrar_barrios.html', \
                items=items, id_usuario=id_usuario, error=error, \
                cadena=cadena)
        elif request.form["action"] == "Eliminar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_eliminar_barrio(seleccionados)
            if error == None: #Si paso las validaciones
                resultado = controllers.eliminar_barrio(seleccionados)
                if resultado == 'OK':
                    flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
                    items = controllers.listar_barrios(distrito)
                else:
                    error = resultado
                    items = controllers.listar_barrios(distrito)

    paginacion_siguiente = utils.paginacion_siguiente(items, \
    pagina, REGISTERS_PER_PAGE)
    paginacion_anterior = utils.paginacion_anterior(items, pagina)
    paginacion_primero = utils.paginacion_primero(items, pagina)
    paginacion_ultimo = utils.paginacion_ultimo(items, pagina, \
    REGISTERS_PER_PAGE)

    items = utils.paginacion(items, pagina, REGISTERS_PER_PAGE)

    return render_template('tbls1/administrar_barrios.html', items=items, \
    id_usuario=id_usuario, cadena=cadena, set_etc=campo, error=error, \
    pg_ante=paginacion_anterior, pg_sgte=paginacion_siguiente, \
    pg_prim=paginacion_primero, pg_ultm=paginacion_ultimo, dist=distrito)

@mod_tbls1.route('/alta_barrios/<distrito>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMLOC')
def crear_barrios(distrito):
    id_usuario = session['id_usuario']
    error = None
    if request.method == 'POST':
        if request.form["action"] == "Salir":
            return redirect(url_for('tbls1.administrar_barrios', \
            distrito=distrito))

        p_codigo = request.form['codigo']
        p_nombre = request.form['nombre']

        if not controllers.verificar_datos_requeridos_alta_barr(p_nombre):
            error = "Datos requeridos no ingresados"
            return render_template('tbls1/crear_barrios.html', \
            id_usuario=id_usuario, error=error, codigo=p_codigo, nombre=p_nombre)

        resultado = controllers.insertar_barrio(distrito, p_codigo, p_nombre)

        if resultado == 'OK':
            flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
            if request.form["action"] == "Guardar y Continuar":
                return render_template('tbls1/crear_barrios.html', \
                id_usuario=id_usuario, error=error)
            else: #Eligio Guardar
                return redirect(url_for('tbls1.administrar_barrios', \
                distrito=distrito))
        else:
            error = resultado
            return render_template('tbls1/crear_barrios.html', \
            id_usuario=id_usuario, error=error, codigo=p_codigo, nombre=p_nombre)

    return render_template('tbls1/crear_barrios.html', id_usuario = \
    id_usuario, error=error, codigo="", nombre="")

@mod_tbls1.route('/modificacion_barrios/<barrmodif>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMLOC')
def modificar_barrios(barrmodif):
    id_usuario = session['id_usuario']
    error = None
    if request.method == 'POST':
        if request.form["action"] == "Salir":
            barr = controllers.obtener_barrio(barrmodif)
            distrito = barr.get('distrito')
            return redirect(url_for('tbls1.administrar_barrios', \
            distrito=distrito))

        p_codigo= request.form['codigo']
        p_nombre = request.form['nombre']

        if not controllers.verificar_datos_requeridos_modif_barr(\
        p_nombre):
            error = "Datos requeridos no ingresados"
            return render_template('tbls1/modificar_barrios.html', \
            id_usuario=id_usuario, error=error, codigo=p_codigo, nombre=p_nombre)

        resultado = controllers.modificar_barrio(barrmodif, p_codigo, p_nombre)

        if resultado == 'OK':
            flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
            barr = controllers.obtener_barrio(barrmodif)
            distrito = barr.get('distrito')
            return redirect(url_for('tbls1.administrar_barrios',\
            distrito=distrito))
        else:
            error = resultado
            return render_template('tbls1/modificar_barrios.html', \
            id_usuario=id_usuario, error=error, codigo=p_codigo, nombre=p_nombre)

    barr = controllers.obtener_barrio(barrmodif)

    if barr == None:
        error = "El barrio no existe"
        return render_template('tbls1/modificar_barrios.html', \
        id_usuario=id_usuario, error=error, codigo="", nombre="")

    return render_template('tbls1/modificar_barrios.html', \
    id_usuario = id_usuario, error=error, codigo=barr.get('codigo'), \
    nombre=barr.get('nombre'))

@mod_tbls1.route('/administrar_planillas', methods=['GET', 'POST'])
@mod_tbls1.route('/administrar_planillas/pagina/<int:pagina>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMPLA')
def administrar_planillas(pagina=1):
    items = []
    error = None
    id_usuario = session['id_usuario']
    session['pagina_origen'] = pagina
    cadena = ""
    campos = "todos"
    if request.method == 'GET':
        busqueda = session['busqueda']
        if busqueda == True:
            cadena = session["cadena"]
            campo = session["campos"]
            items = controllers.buscar_planillas(cadena, campo)
        else:
            items = controllers.listar_planillas()
    elif request.method == 'POST':
        if request.form["action"] == "Buscar":
            cadena = request.form["cadena"]
            campos = request.form["campos"]
            session['cadena'] = cadena
            session['campos'] = campo
            items = controllers.buscar_planillas(cadena, campos)
            if len(items) == 0:
                session['busqueda'] = False
                flash("No se encontraron coincidencias")
            else:
                session['busqueda'] = True
        elif request.form["action"] == "Limpiar":
            session['busqueda'] = False
            pagina = 1
            items = controllers.listar_planillas()
        elif request.form["action"] == "Eliminar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_eliminar_planilla(seleccionados)
            if error == None: #Si paso las validaciones
                resultado = controllers.eliminar_planilla(seleccionados)
                if resultado == 'OK':
                    flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
                    items = controllers.listar_planillas()
                else:
                    error = resultado
        elif request.form["action"] == "Salir":
            session['busqueda'] = False
            return redirect(url_for('auth.menu_principal'))
        elif request.form["action"] == "Detalles":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_detalles_planilla(seleccionados)
            if error == None: #Si paso las validaciones
                return redirect(url_for('tbls1.detalles_planilla', tbls1_id=seleccionados[0]))

    paginacion_siguiente = utils.paginacion_siguiente(items, \
    pagina, REGISTERS_PER_PAGE)
    paginacion_anterior = utils.paginacion_anterior(items, pagina)
    paginacion_primero = utils.paginacion_primero(items, pagina)
    paginacion_ultimo = utils.paginacion_ultimo(items, pagina, \
    REGISTERS_PER_PAGE)
    
    items = utils.paginacion(items, pagina, REGISTERS_PER_PAGE)

    return render_template('tbls1/administrar_tbls1.html', items=items, \
    id_usuario=id_usuario, error=error, cadena=cadena, set_etc=campos, \
    pg_ante=paginacion_anterior, pg_sgte=paginacion_siguiente, \
    pg_prim=paginacion_primero, pg_ultm=paginacion_ultimo)

@mod_tbls1.route('/detalles_planilla/<tbls1_id>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMPLA')
def detalles_planilla(tbls1_id):
    id_usuario = session['id_usuario']
    error = None
    if request.method == 'POST':
        if request.form["action"] == "Salir":
            return redirect(url_for('tbls1.administrar_planillas', pagina=session['pagina_origen']))

    planilla = controllers.obtener_planilla(tbls1_id)

    if planilla == None:
        error = "La planilla no existe"
        return render_template('tbls1/detalles_tbls1.html', \
        id_usuario=id_usuario, error=error, departamento="", \
        distrito="", barrio="", usuario="", fecha="")

    return render_template('tbls1/detalles_tbls1.html', \
    id_usuario = id_usuario, error=error, \
    departamento=planilla.get('departamento'), \
    distrito=planilla.get('distrito'),
    barrio=planilla.get('barrio'),
    usuario=planilla.get('usuario'),
    fecha=planilla.get('fecha'))

@mod_tbls1.route('/administrar_registros/<planilla>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMPLA')
def administrar_registros(planilla):
    items = []
    error = None
    id_usuario = session['id_usuario']
    cadena = ""
    campos = "todos"
    if request.method == 'GET':
        busqueda = session['busqueda']
        if busqueda == True:
            cadena = session["cadena"]
            campo = session["campos"]
            items = controllers.buscar_registros(cadena, campo, planilla)
        else:
            items = controllers.listar_registros(planilla)
    elif request.method == 'POST':
        if request.form["action"] == "Buscar":
            cadena = request.form["cadena"]
            campos = request.form["campos"]
            session['cadena'] = cadena
            session['campos'] = campo
            items = controllers.buscar_registros(cadena, campos, planilla)
            if len(items) == 0:
                session['busqueda'] = False
                flash("No se encontraron coincidencias")
            else:
                session['busqueda'] = True
        elif request.form["action"] == "Limpiar":
            session['busqueda'] = False
            items = controllers.listar_registros(planilla)
        elif request.form["action"] == "Eliminar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_eliminar_registro(seleccionados)
            if error == None: #Si paso las validaciones
                resultado = controllers.eliminar_registro(seleccionados)
                if resultado == 'OK':
                    flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
                    items = controllers.listar_registros(planilla)
                else:
                    error = resultado
        elif request.form["action"] == "Salir":
            session['busqueda'] = False
            return redirect(url_for('tbls1.administrar_planillas', pagina=session['pagina_origen']))
        elif request.form["action"] == "Detalles":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_detalles_registro(seleccionados)
            if error == None: #Si paso las validaciones
                return redirect(url_for('tbls1.detalles_registro', registro_id=seleccionados[0]))

    return render_template('tbls1/administrar_registros1.html', items=items, \
    id_usuario=id_usuario, error=error, cadena=cadena, set_etc=campos)

@mod_tbls1.route('/detalles_registro/<registro_id>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMPLA')
def detalles_registro(registro_id):
    id_usuario = session['id_usuario']
    error = None
    if request.method == 'POST':
        if request.form["action"] == "Salir":
            registro = controllers.obtener_registro(registro_id)
            planilla = registro.get('tabla_s1')
            return redirect(url_for('tbls1.administrar_registros', \
            planilla=planilla))

    registro = controllers.obtener_registro(registro_id)
    criaderosA = controllers.listar_criaderos_a(registro_id)
    criaderosB = controllers.listar_criaderos_b(registro_id)
    criaderosC = controllers.listar_criaderos_c(registro_id)

    if registro == None:
        error = "El registro no existe"
        return render_template('tbls1/detalles_registros1.html', \
        id_usuario=id_usuario, error=error, manzana="", \
        direccion="", latitud="", longitud="")

    return render_template('tbls1/detalles_registros1.html', \
    id_usuario = id_usuario, error=error, \
    manzana=registro.get('manzana'), \
    direccion=registro.get('direccion'), \
    latitud=registro.get('latitud'), \
    longitud=registro.get('longitud'), \
    criaderosA=criaderosA, \
    criaderosB=criaderosB, \
    criaderosC=criaderosC)

@mod_tbls1.route('/administrar_tipo_control', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMTCTRL')
def administrar_tipo_control():
    items = []
    error = None
    id_usuario = session['id_usuario']
    cadena = ""
    campo = "todos"
    if request.method == 'GET':
        busqueda = session['busqueda']
        if busqueda == True:
            cadena = session["cadena"]
            campo = session["campos"]
            items = controllers.buscar_tipo_control(cadena)
        else:
            items = controllers.listar_tipo_control()
    elif request.method == 'POST':
        if request.form["action"] == "Buscar":
            cadena = request.form["cadena"]
            session['cadena'] = cadena
            items = controllers.buscar_tipo_control(cadena)
            if len(items) == 0:
                session['busqueda'] = False
                flash("No se encontraron coincidencias")
            else:
                session['busqueda'] = True
        elif request.form["action"] == "Limpiar":
            session['busqueda'] = False
            items = controllers.listar_tipo_control()
        elif request.form["action"] == "Salir":
            session['busqueda'] = False
            return redirect(url_for('auth.menu_principal'))
        elif request.form["action"] == "Crear":
            return redirect(url_for('tbls1.alta_tipo_control'))
        elif request.form["action"] == "Modificar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_modificar_tipo_control(seleccionados)
            if error == None: #Si paso las validaciones
                return redirect(url_for('tbls1.modificacion_tipo_control', tcont=seleccionados[0]))
            else: #No paso las validaciones
                return render_template('tbls1/administrar_tipo_control.html', \
                items=items, id_usuario=id_usuario, error=error, \
                cadena=cadena)
        elif request.form["action"] == "Eliminar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_eliminar_tipo_control(seleccionados)
            if error == None: #Si paso las validaciones
                resultado = controllers.eliminar_tipo_control(seleccionados)
                if resultado == 'OK':
                    flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
                    items = controllers.listar_tipo_control()
                else:
                    error = resultado
                    items = controllers.listar_tipo_control()

    return render_template('tbls1/administrar_tipo_control.html', items=items, \
    id_usuario=id_usuario, cadena=cadena, set_etc=campo, error=error)

@mod_tbls1.route('/alta_tipo_control', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMTCTRL')
def alta_tipo_control():
    id_usuario = session['id_usuario']
    error = None
    if request.method == 'POST':
        if request.form["action"] == "Salir":
            return redirect(url_for('tbls1.administrar_tipo_control'))

        p_nombre = request.form['nombre']

        if not controllers.verificar_datos_requeridos_alta_tcont(p_nombre):
            error = "Datos requeridos no ingresados"
            return render_template('tbls1/crear_tipo_control.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre)

        resultado = controllers.insertar_tipo_control(p_nombre)

        if resultado == 'OK':
            flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
            if request.form["action"] == "Guardar y Continuar":
                return render_template('tbls1/crear_tipo_control.html', \
                id_usuario=id_usuario, error=error)
            else: #Eligio Guardar
                return redirect(url_for('tbls1.administrar_tipo_control'))
        else:
            error = resultado
            return render_template('tbls1/crear_tipo_control.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre)

    return render_template('tbls1/crear_tipo_control.html', id_usuario = \
    id_usuario, error=error, nombre="")

@mod_tbls1.route('/modificacion_tipo_control/<tcont>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMTCTRL')
def modificacion_tipo_control(tcont):
    id_usuario = session['id_usuario']
    error = None
    if request.method == 'POST':
        if request.form["action"] == "Salir":
            return redirect(url_for('tbls1.administrar_tipo_control'))

        p_nombre = request.form['nombre']

        if not controllers.verificar_datos_requeridos_modif_tcont(\
        p_nombre):
            error = "Datos requeridos no ingresados"
            return render_template('tbls1/modificar_tipo_control.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre)

        resultado = controllers.modificar_tipo_control(tcont, p_nombre)

        if resultado == 'OK':
            flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
            return redirect(url_for('tbls1.administrar_tipo_control'))
        else:
            error = resultado
            return render_template('tbls1/modificar_tipo_control.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre)

    tipo_control = controllers.obtener_tipo_control(tcont)

    if tipo_control == None:
        error = "El tipo de control no existe"
        return render_template('tbls1/modificar_tipo_control.html', \
        id_usuario=id_usuario, error=error, nombre="")

    return render_template('tbls1/modificar_tipo_control.html', \
    id_usuario = id_usuario, error=error, \
    nombre=tipo_control.get('nombre'))

@mod_tbls1.route('/administrar_tipo_predio', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMTPRE')
def administrar_tipo_predio():
    items = []
    error = None
    id_usuario = session['id_usuario']
    cadena = ""
    campo = "todos"
    items = controllers.listar_tipo_predio()
    if request.method == 'GET':
        busqueda = session['busqueda']
        if busqueda == True:
            cadena = session["cadena"]
            campo = session["campos"]
            items = controllers.buscar_tipo_predio(cadena)
        else:
            items = controllers.listar_tipo_predio()
    elif request.method == 'POST':
        if request.form["action"] == "Buscar":
            cadena = request.form["cadena"]
            session['cadena'] = cadena
            items = controllers.buscar_tipo_predio(cadena)
            if len(items) == 0:
                session['busqueda'] = False
                flash("No se encontraron coincidencias")
            else:
                session['busqueda'] = True
        elif request.form["action"] == "Limpiar":
            session['busqueda'] = False
            items = controllers.listar_tipo_predio()
        elif request.form["action"] == "Salir":
            session['busqueda'] = False
            return redirect(url_for('auth.menu_principal'))
        elif request.form["action"] == "Crear":
            return redirect(url_for('tbls1.alta_tipo_predio'))
        elif request.form["action"] == "Modificar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_modificar_tipo_predio(seleccionados)
            if error == None: #Si paso las validaciones
                return redirect(url_for('tbls1.modificacion_tipo_predio', tpred=seleccionados[0]))
            else: #No paso las validaciones
                return render_template('tbls1/administrar_tipo_predio.html', \
                items=items, id_usuario=id_usuario, error=error, \
                cadena=cadena)
        elif request.form["action"] == "Eliminar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_eliminar_tipo_predio(seleccionados)
            if error == None: #Si paso las validaciones
                resultado = controllers.eliminar_tipo_predio(seleccionados)
                if resultado == 'OK':
                    flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
                    items = controllers.listar_tipo_predio()
                else:
                    error = resultado
                    items = controllers.listar_tipo_predio()

    return render_template('tbls1/administrar_tipo_predio.html', items=items, \
    id_usuario=id_usuario, cadena=cadena, set_etc=campo, error=error)

@mod_tbls1.route('/alta_tipo_predio', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMTPRE')
def alta_tipo_predio():
    id_usuario = session['id_usuario']
    error = None
    if request.method == 'POST':
        if request.form["action"] == "Salir":
            return redirect(url_for('tbls1.administrar_tipo_predio'))

        p_nombre = request.form['nombre']

        if not controllers.verificar_datos_requeridos_alta_tpred(p_nombre):
            error = "Datos requeridos no ingresados"
            return render_template('tbls1/crear_tipo_predio.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre)

        resultado = controllers.insertar_tipo_predio(p_nombre)

        if resultado == 'OK':
            flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
            if request.form["action"] == "Guardar y Continuar":
                return render_template('tbls1/crear_tipo_predio.html', \
                id_usuario=id_usuario, error=error)
            else: #Eligio Guardar
                return redirect(url_for('tbls1.administrar_tipo_predio'))
        else:
            error = resultado
            return render_template('tbls1/crear_tipo_predio.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre)

    return render_template('tbls1/crear_tipo_predio.html', id_usuario = \
    id_usuario, error=error, nombre="")

@mod_tbls1.route('/modificacion_tipo_predio/<tpred>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMTPRE')
def modificacion_tipo_predio(tpred):
    id_usuario = session['id_usuario']
    error = None
    if request.method == 'POST':
        if request.form["action"] == "Salir":
            return redirect(url_for('tbls1.administrar_tipo_predio'))

        p_nombre = request.form['nombre']

        if not controllers.verificar_datos_requeridos_modif_tpred(\
        p_nombre):
            error = "Datos requeridos no ingresados"
            return render_template('tbls1/modificar_tipo_predio.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre)

        resultado = controllers.modificar_tipo_predio(tpred, p_nombre)

        if resultado == 'OK':
            flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
            return redirect(url_for('tbls1.administrar_tipo_predio'))
        else:
            error = resultado
            return render_template('tbls1/modificar_tipo_predio.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre)

    tipo_predio = controllers.obtener_tipo_predio(tpred)

    if tipo_predio == None:
        error = "El tipo de predio no existe"
        return render_template('tbls1/modificar_tipo_predio.html', \
        id_usuario=id_usuario, error=error, nombre="")

    return render_template('tbls1/modificar_tipo_predio.html', \
    id_usuario = id_usuario, error=error, \
    nombre=tipo_predio.get('nombre'))
