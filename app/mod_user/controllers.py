from database import db_session

# Import module models
from app.mod_user.models import Usuario, Rol

from app.mod_auth.controllers import set_password, check_password

from sqlalchemy import exc
from sqlalchemy import or_

from sqlalchemy.sql.expression import func

import datetime

def listar_usuarios():
    usuarios = db_session.query(Usuario).all()
    items = []
    for fila_usuario in usuarios:
        item = dict(id=fila_usuario.id, \
        id_usuario=fila_usuario.id_usuario, nombre=fila_usuario.nombre, \
        apellido=fila_usuario.apellido, estado=fila_usuario.estado)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['id_usuario'])
    return items

def obtener_usuario(identificador):
    item = None
    contador = db_session.query(Usuario).filter_by(id=identificador).count()

    if contador > 0:
        usr = db_session.query(Usuario).filter_by(id=identificador).one()

        item = dict(nombre=usr.nombre, apellido=usr.apellido, \
        fecha_nacimiento=usr.fecha_nacimiento.strftime('%d/%m/%Y'))

    return item

def obtener_id(id_usuario):
    item = None
    contador = db_session.query(Usuario).filter_by(id_usuario=id_usuario).count()

    if contador > 0:
        usr = db_session.query(Usuario).filter_by(id_usuario=id_usuario).one()

        item = usr.id

    return item

def buscar_usuarios(cadena, campos):
    items = []
    cadena_lwr = cadena.lower()

    if campos == "todos":
        usuarios = db_session.query(Usuario).filter\
        (or_(func.lower(Usuario.id_usuario).contains(cadena_lwr), \
        func.lower(Usuario.nombre).contains(cadena_lwr), \
        func.lower(Usuario.apellido).contains(cadena_lwr))).all()
    elif campos == "usuario":
        usuarios = db_session.query(Usuario).filter\
        (func.lower(Usuario.id_usuario).contains(cadena_lwr)).all()
    elif campos == "nombre":
        usuarios = db_session.query(Usuario).filter\
        (func.lower(Usuario.nombre).contains(cadena_lwr)).all()
    elif campos == "apellido":
        usuarios = db_session.query(Usuario).filter\
        (func.lower(Usuario.apellido).contains(cadena_lwr)).all()

    for fila_usuario in usuarios:
        item = dict(id=fila_usuario.id, \
        id_usuario=fila_usuario.id_usuario, nombre=fila_usuario.nombre, \
        apellido=fila_usuario.apellido, estado=fila_usuario.estado)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['id_usuario'])
    return items

def verificar_datos_requeridos_alta(id_usuario, nombre, apellido, \
contrasena, contrasena2, fecha_nac):
    sw = True

    if id_usuario == "":
        sw = False
    elif contrasena == "":
        sw = False
    elif contrasena2 == "":
        sw = False
    elif nombre == "":
        sw = False
    elif apellido == "":
        sw = False
    elif fecha_nac == "":
        sw = False
    return sw

def verificar_contrasena(pass1, pass2):
    return pass1 == pass2

def insertar_usuario(id_usuario, contrasena, nombre, apellido, \
fecha_nacimiento, usuario_alta):
    contador = db_session.query(Usuario). \
    filter_by(id_usuario=id_usuario).count()

    if contador > 0:
        return 'El usuario ya existe'

    try:
        usuario = Usuario()
        usuario.id_usuario = id_usuario
        usuario.contrasena = set_password(contrasena)
        usuario.nombre = nombre
        usuario.apellido = apellido
        usuario.fecha_nacimiento = datetime.datetime.strptime(fecha_nacimiento, '%d/%m/%Y')
        usuario.fecha_creacion = datetime.datetime.now()
        usuario.usuario_creacion = usuario_alta
        usuario.fecha_modificacion = datetime.datetime.now()
        usuario.usuario_modificacion = usuario_alta
        usuario.estado = 'ACTIVO'
        db_session.add(usuario)
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def validaciones_modificar_usuario(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun usuario
        error = "Debe seleccionar un usuario"
    elif len(seleccionados) > 1: #Selecciono mas de un usuario
        error = u"Debe seleccionar un \xfanico usuario"

    return error

def verificar_datos_requeridos_modificacion(nombre, apellido, fecha_nac):
    sw = True

    if nombre == "":
        sw = False
    elif apellido == "":
        sw = False
    elif fecha_nac == "":
        sw = False
    return sw

def modificar_usuario(identificador, nombre, apellido, fecha_nacimiento,\
usuario_modificacion):
    try:
        #Usuario a ser modificado
        usuario = db_session.query(Usuario).filter_by(id=\
        identificador).one()

        usuario.nombre = nombre
        usuario.apellido = apellido
        usuario.fecha_nacimiento = datetime.datetime.strptime(fecha_nacimiento, '%d/%m/%Y')
        usuario.fecha_modificacion = datetime.datetime.now()
        usuario.usuario_modificacion = usuario_modificacion
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def verificar_datos_requeridos_contrasena(passanterior, passnueva, passnueva2):
    sw = True

    if passanterior == "":
        sw = False
    elif passnueva == "":
        sw = False
    elif passnueva2 == "":
        sw = False
    return sw

def modificar_contrasena(id_usuario, passanterior, passnueva, passnueva2):
    try:
        usuario = db_session.query(Usuario).\
        filter_by(id_usuario=id_usuario).one()

        usuario_contrasena = usuario.contrasena

        if not check_password(usuario_contrasena, passanterior):
            return u'La contrase\xf1a anterior no es correcta'

        if passnueva != passnueva2:
            return u'Las contrase\xf1as nuevas ingresadas no son iguales'

        usuario.contrasena = set_password(passnueva)
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def verificar_datos_requeridos_reestablecer(passnueva, passnueva2):
    sw = True

    if passnueva == "":
        sw = False
    elif passnueva2 == "":
        sw = False
    return sw

def reestablecer_contrasena(identificador, passnueva, passnueva2):
    try:
        usuario = db_session.query(Usuario).\
        filter_by(id=identificador).one()

        if passnueva != passnueva2:
            return u'Las contrase\xf1as nuevas ingresadas no son iguales'

        usuario.contrasena = set_password(passnueva)
        db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message

    return 'OK'

def validaciones_eliminar_activar_usuario(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun usuario
        error = "Debe seleccionar al menos un usuario"

    return error

def eliminar_activar_usuario(seleccionados, usuario_modificacion, accion):
    try:
        if accion == 'Eliminar':
            estado = 'INACTIVO'
        elif accion == 'Activar':
            estado = 'ACTIVO'

        for elemento in seleccionados:
            usuario = db_session.query(Usuario).filter_by(id=\
            elemento).one()
            usuario.estado = estado
            usuario.usuario_modificacion = usuario_modificacion
            usuario.fecha_modificacion = datetime.datetime.now()
            db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message
    return 'OK'

def obtener_roles_usuario(identificador):
    items = []

    contador = db_session.query(Usuario).filter_by(id=\
    identificador).count()

    if contador == 0:
        return items

    usuario = db_session.query(Usuario).filter_by(id=\
    identificador).one()

    roles = usuario.roles

    for fila_rol in roles:
        item = dict(id=fila_rol.id, \
        nombre=fila_rol.nombre, descripcion=fila_rol.descripcion)

        items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def buscar_roles_usuario(cadena, campos, identificador_usr):
    items = []
    cadena_lwr = cadena.lower()

    if campos == "todos":
        roles = db_session.query(Rol).filter\
        (or_(func.lower(Rol.nombre).contains(cadena_lwr), \
        func.lower(Rol.descripcion).contains(cadena_lwr))).all()
    elif campos == "nombre":
        roles = db_session.query(Rol).filter\
        (func.lower(Rol.nombre).contains(cadena_lwr)).all()
    elif campos == "descripcion":
        roles = db_session.query(Rol).filter\
        (func.lower(Rol.descripcion).contains(cadena_lwr)).all()

    contador = db_session.query(Usuario). \
    filter_by(id=identificador_usr).count()

    if contador == 0:
        return []

    usuario = db_session.query(Usuario). \
    filter_by(id=identificador_usr).one()

    for fila_rol in roles:
        for rol_usuario in usuario.roles:
            if fila_rol.id == rol_usuario.id:
                item = dict(id=fila_rol.id, \
                nombre=fila_rol.nombre, descripcion=\
                fila_rol.descripcion)

                items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def obtener_roles_usuario_asignar(identificador):
    items = []

    contador = db_session.query(Usuario).filter_by(id=\
    identificador).count()

    if contador == 0:
        return items

    usuario = db_session.query(Usuario).filter_by(id=\
    identificador).one()

    roles = db_session.query(Rol).all()

    for fila_rol in roles:
        continuar = True
        for rol_usuario in usuario.roles:
            if fila_rol.id == rol_usuario.id:
                continuar = False
                break

        if continuar:
            item = dict(id=fila_rol.id, \
            nombre=fila_rol.nombre, descripcion=\
            fila_rol.descripcion)

            items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def buscar_roles_usuario_asignar(cadena, campos, identificador_usr):
    items = []
    cadena_lwr = cadena.lower()

    if campos == "todos":
        roles = db_session.query(Rol).filter\
        (or_(func.lower(Rol.nombre).contains(cadena_lwr), \
        func.lower(Rol.descripcion).contains(cadena_lwr))).all()
    elif campos == "nombre":
        roles = db_session.query(Rol).filter\
        (func.lower(Rol.nombre).contains(cadena_lwr)).all()
    elif campos == "descripcion":
        roles = db_session.query(Rol).filter\
        (func.lower(Rol.descripcion).contains(cadena_lwr)).all()

    contador = db_session.query(Usuario). \
    filter_by(id=identificador_usr).count()

    if contador == 0:
        return []

    usuario = db_session.query(Usuario). \
    filter_by(id=identificador_usr).one()

    for fila_rol in roles:
        continuar = True
        for rol_usuario in usuario.roles:
            if fila_rol.id == rol_usuario.id:
                continuar = False
                break

        if continuar:
            item = dict(id=fila_rol.id, \
            nombre=fila_rol.nombre, descripcion=\
            fila_rol.descripcion)

            items.append(item)

    items = sorted(items, key=lambda elemento: elemento['nombre'])
    return items

def validaciones_asignar_roles_usuario(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun rol
        error = "Debe seleccionar al menos un rol"

    return error

def asignar_roles_usuario(seleccionados, usuariorol):
    try:
        usuario = db_session.query(Usuario).filter_by(id=\
        usuariorol).one()
        for elemento in seleccionados:
            rol = db_session.query(Rol).filter_by(id=\
            elemento).one()
            usuario.roles.append(rol)
            db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message
    return 'OK'

def validaciones_quitar_roles_usuario(seleccionados):
    error = None
    if len(seleccionados) == 0: #No selecciono ningun rol
        error = "Debe seleccionar al menos un rol"

    return error

def quitar_roles_usuario(seleccionados, usuariorol):
    try:
        usuario = db_session.query(Usuario).filter_by(id=\
        usuariorol).one()
        for elemento in seleccionados:
            rol = db_session.query(Rol).filter_by(id=\
            elemento).one()
            usuario.roles.remove(rol)
            db_session.flush()
        db_session.commit()
    except exc.SQLAlchemyError, e:
        db_session.rollback()
        return e.message
    return 'OK'
