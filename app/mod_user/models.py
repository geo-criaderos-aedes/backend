from sqlalchemy import Column, DateTime, ForeignKey, Integer, String, Table, Unicode
from database import Base

from sqlalchemy.orm import relationship

usuario_rol = Table('usuario_rol', Base.metadata, 
    Column('usuario_id', Integer, ForeignKey('usuario.id')), 
    Column('rol_id', Integer, ForeignKey('rol.id'))
)

rol_permiso = Table('rol_permiso', Base.metadata, 
    Column('rol_id', Integer, ForeignKey('rol.id')), 
    Column('permiso_id', Integer, ForeignKey('permiso.id'))
)

class Usuario(Base):
    __tablename__ = 'usuario'

    id = Column(Integer, primary_key=True)
    id_usuario = Column(String(15), unique=True, nullable=False)
    contrasena = Column(Unicode, nullable=False)
    nombre = Column(String(30), nullable=False)
    apellido = Column(String(30), nullable=False)
    fecha_nacimiento = Column(DateTime, nullable=False)
    fecha_creacion = Column(DateTime, nullable=False)
    usuario_creacion = Column(Integer, nullable=False)
    fecha_modificacion = Column(DateTime, nullable=False)
    usuario_modificacion = Column(Integer, nullable=False)
    estado = Column(String(10), nullable=False)
    roles = relationship('Rol', secondary=usuario_rol,
        backref='usuarios')

class Rol(Base):
    __tablename__ = 'rol'

    id = Column(Integer, primary_key=True)
    nombre = Column(String(15), nullable=False)
    descripcion = Column(String(30))
    permisos = relationship('Permiso', secondary=rol_permiso,
        backref='roles')

class Permiso(Base):
    __tablename__ = 'permiso'

    id = Column(Integer, primary_key=True)
    nombre = Column(String(15), nullable=False)
    descripcion = Column(String(30))
