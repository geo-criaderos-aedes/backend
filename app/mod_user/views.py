# Import flask dependencies
from flask import Blueprint
from flask import flash
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from functools import wraps

from app import login_required
from app.mod_user import controllers
from app.mod_auth import controllers as ctrl_auth

from config import REGISTERS_PER_PAGE
import utils

def _get_unauthorized_view():
    return redirect(url_for('no_autorizado'))

def roles_required(*roles):
    """Decorador que especifica que un usuario debe tener todos los 
    roles especificados."""
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            id_usuario = session['id_usuario']
            continuar = ctrl_auth.roles_required(id_usuario, roles)
            if(continuar):
                return fn(*args, **kwargs)
            return _get_unauthorized_view()
        return decorated_view
    return wrapper

def roles_accepted(*roles):
    """Decorador que especifica que un usuario debe tener al menos uno
    de los roles especificados"""
    def wrapper(fn):
        @wraps(fn)
        def decorated_view(*args, **kwargs):
            id_usuario = session['id_usuario']
            continuar = ctrl_auth.roles_accepted(id_usuario, roles)
            if(continuar):
                return fn(*args, **kwargs)
            return _get_unauthorized_view()
        return decorated_view
    return wrapper

# Define the blueprint
mod_user = Blueprint('usuarios', __name__, url_prefix='/usuarios')

@mod_user.route('/', methods=['GET', 'POST'])
@mod_user.route('/pagina/<int:pagina>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMUSR')
def administrar_usuarios(pagina=1):
    items = []
    error = None
    id_usuario = session['id_usuario']
    cadena = ""
    campo = "todos"
    if request.method == 'GET':
        busqueda = session['busqueda']
        if busqueda == True:
            cadena = session["cadena"]
            campo = session["campos"]
            items = controllers.buscar_usuarios(cadena, campo)
        else:
            items = controllers.listar_usuarios()
    elif request.method == 'POST':
        if request.form["action"] == "Buscar":
            cadena = request.form["cadena"]
            campo = request.form["campos"]
            session['cadena'] = cadena
            session['campos'] = campo
            items = controllers.buscar_usuarios(cadena, campo)
            if len(items) == 0:
                session['busqueda'] = False
                flash("No se encontraron coincidencias")
            else:
                session['busqueda'] = True
        elif request.form["action"] == "Limpiar":
            session['busqueda'] = False
            pagina = 1
            items = controllers.listar_usuarios()
        elif request.form["action"] == "Salir":
            session['busqueda'] = False
            return redirect(url_for('auth.menu_principal'))
        elif request.form["action"] == "Crear":
            return redirect(url_for('usuarios.crear_usuarios'))
        elif request.form["action"] == "Modificar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_modificar_usuario(seleccionados)
            if error == None: #Si paso las validaciones
                return redirect(url_for('usuarios.modificar_usuarios', usermodif=seleccionados[0]))
        elif request.form["action"] == "Eliminar" or request.form["action"] == "Activar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_eliminar_activar_usuario(seleccionados)
            if error == None: #Si paso las validaciones
                id_usr_modificador = controllers.obtener_id(id_usuario)
                accion = request.form["action"]
                resultado = controllers.eliminar_activar_usuario(seleccionados, \
                id_usr_modificador, accion)
                if resultado == 'OK':
                    flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
                    items = controllers.listar_usuarios()
                else:
                    error = resultado

    paginacion_siguiente = utils.paginacion_siguiente(items, \
    pagina, REGISTERS_PER_PAGE)
    paginacion_anterior = utils.paginacion_anterior(items, pagina)
    paginacion_primero = utils.paginacion_primero(items, pagina)
    paginacion_ultimo = utils.paginacion_ultimo(items, pagina, \
    REGISTERS_PER_PAGE)

    items = utils.paginacion(items, pagina, REGISTERS_PER_PAGE)

    return render_template('user/administrar_usuarios.html', items=items, \
    id_usuario=id_usuario, error=error, cadena=cadena, set_etc=campo,\
    pg_ante=paginacion_anterior, pg_sgte=paginacion_siguiente, \
    pg_prim=paginacion_primero, pg_ultm=paginacion_ultimo)

@mod_user.route('/alta', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMUSR')
def crear_usuarios():
    id_usuario = session['id_usuario']
    error = None
    if request.method == 'POST':
        if request.form["action"] == "Salir":
            return redirect(url_for('usuarios.administrar_usuarios'))

        p_usuario = request.form['usuario']
        p_contrasena = request.form['contrasena']
        p_contrasena2 = request.form['contrasena2']
        p_nombre = request.form['nombre']
        p_apellido = request.form['apellido']
        p_fecha_nac = request.form['fecha_nac']

        if not controllers.verificar_datos_requeridos_alta(p_usuario, \
        p_nombre, p_apellido, p_contrasena, p_contrasena2, p_fecha_nac):
            error = "Datos requeridos no ingresados"
            return render_template('user/crear_usuarios.html', \
            id_usuario=id_usuario, error=error, usuario=p_usuario, \
            contrasena=p_contrasena, contrasena2=p_contrasena2, \
            nombre=p_nombre, apellido=p_apellido, fecha_nac=p_fecha_nac)

        if not controllers.verificar_contrasena(p_contrasena, p_contrasena2):
            error = u"Las contrase\xf1as no coinciden"
            return render_template('user/crear_usuarios.html', \
            id_usuario=id_usuario, error=error, usuario=p_usuario, \
            contrasena=p_contrasena, contrasena2=p_contrasena2, \
            nombre=p_nombre, apellido=p_apellido, fecha_nac=p_fecha_nac)

        id_usr_creador = controllers.obtener_id(id_usuario)

        resultado = controllers.insertar_usuario(p_usuario, \
        p_contrasena, p_nombre, p_apellido, p_fecha_nac, id_usr_creador)

        if resultado == 'OK':
            flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
            if request.form["action"] == "Guardar y Continuar":
                return render_template('user/crear_usuarios.html', \
                id_usuario=id_usuario, error=error)
            else: #Eligio Guardar
                return redirect(url_for('usuarios.administrar_usuarios'))
        else:
            error = resultado
            return render_template('user/crear_usuarios.html', \
            id_usuario=id_usuario, error=error, usuario=p_usuario, \
            contrasena=p_contrasena, contrasena2=p_contrasena2, \
            nombre=p_nombre, apellido=p_apellido, fecha_nac=p_fecha_nac)

    return render_template('user/crear_usuarios.html', id_usuario = \
    id_usuario, error=error, usuario="", contrasena="", contrasena2="", \
    nombre="", apellido="", fecha_nac="")

@mod_user.route('/modificacion/<usermodif>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMUSR')
def modificar_usuarios(usermodif):
    id_usuario = session['id_usuario']
    error = None
    if request.method == 'POST':
        if request.form["action"] == "Salir":
            return redirect(url_for('usuarios.administrar_usuarios'))
        elif request.form["action"] == "Reestablecer":
            return redirect(url_for('usuarios.reestablecer_contrasena', \
            usermodif=usermodif))

        p_nombre = request.form['nombre']
        p_apellido = request.form['apellido']
        p_fecha_nac = request.form['fecha_nac']

        if not controllers.verificar_datos_requeridos_modificacion(\
        p_nombre, p_apellido, p_fecha_nac):
            error = "Datos requeridos no ingresados"
            return render_template('user/modificar_usuarios.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre, \
            apellido=p_apellido, fecha_nac=p_fecha_nac)

        id_usr_modificador = controllers.obtener_id(id_usuario)

        resultado = controllers.modificar_usuario(usermodif, \
        p_nombre, p_apellido, p_fecha_nac, id_usr_modificador)

        if resultado == 'OK':
            flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
            return redirect(url_for('usuarios.administrar_usuarios'))
        else:
            error = resultado
            return render_template('user/modificar_usuarios.html', \
            id_usuario=id_usuario, error=error, nombre=p_nombre, \
            apellido=p_apellido, fecha_nac=p_fecha_nac)

    usr = controllers.obtener_usuario(usermodif)

    if usr == None:
        error = "El usuario no existe"
        return render_template('user/modificar_usuarios.html', \
        id_usuario=id_usuario, error=error, nombre="", apellido="",\
        fecha_nac="")

    return render_template('user/modificar_usuarios.html', \
    id_usuario = id_usuario, error=error, nombre=usr.get('nombre'), \
    apellido=usr.get('apellido'), fecha_nac=usr.get('fecha_nacimiento'))

@mod_user.route('/modificacion/reestablecer_contrasena/<usermodif>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMUSR')
def reestablecer_contrasena(usermodif):
    id_usuario = session['id_usuario']
    error = None
    if request.method == 'POST':
        if request.form["action"] == "Salir":
            return redirect(url_for('usuarios.administrar_usuarios'))

        p_passnueva = request.form['passnueva']
        p_passnueva2 = request.form['passnueva2']

        if not controllers.verificar_datos_requeridos_reestablecer\
        (p_passnueva, p_passnueva2):
            error = "Datos requeridos no ingresados"
            return render_template('user/reestablecer_contrasena.html', \
            error=error, id_usuario=id_usuario, passnueva=p_passnueva, \
            passnueva2=p_passnueva2)

        resultado = controllers.reestablecer_contrasena(usermodif, \
        p_passnueva, p_passnueva2)

        if resultado == 'OK':
            flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
            return redirect(url_for('usuarios.administrar_usuarios'))
        else:
            error = resultado
            return render_template('user/reestablecer_contrasena.html', \
            error=error, id_usuario=id_usuario, passnueva=p_passnueva, \
            passnueva2=p_passnueva2)

    return render_template('user/reestablecer_contrasena.html', \
    error=error, id_usuario=id_usuario, passnueva="", passnueva2="")

@mod_user.route('/contrasena', methods=['GET', 'POST'])
@login_required
def modificar_contrasena():
    id_usuario = session['id_usuario']
    error = None
    if request.method == 'POST':
        if request.form["action"] == "Salir":
            return redirect(url_for('auth.menu_principal'))

        p_passanterior = request.form['passanterior']
        p_passnueva = request.form['passnueva']
        p_passnueva2 = request.form['passnueva2']

        if not controllers.verificar_datos_requeridos_contrasena\
        (p_passanterior, p_passnueva, p_passnueva2):
            error = "Datos requeridos no ingresados"
            return render_template('user/modificar_contrasena.html', \
            error=error, id_usuario=id_usuario, passanterior=p_passanterior, \
            passnueva=p_passnueva, passnueva2=p_passnueva2)

        resultado = controllers.modificar_contrasena(id_usuario, \
        p_passanterior, p_passnueva, p_passnueva2)

        if resultado == 'OK':
            flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
            return redirect(url_for('auth.menu_principal'))
        else:
            error = resultado
            return render_template('user/modificar_contrasena.html', \
            error=error, id_usuario=id_usuario, passanterior=p_passanterior, \
            passnueva=p_passnueva, passnueva2=p_passnueva2)

    return render_template('user/modificar_contrasena.html', \
    error=error, id_usuario=id_usuario, passanterior="", passnueva="", \
    passnueva2="")

@mod_user.route('/roles/<usuarioroles>/<int:pagina>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMUSR')
def roles_usuario(usuarioroles, pagina):
    id_usuario = session['id_usuario']
    items = []
    error = None
    cadena = ""
    campo = "todos"
    if request.method == 'GET':
        busqueda = session['busqueda']
        if busqueda == True:
            cadena = session["cadena"]
            campo = session["campos"]
            items = controllers.buscar_roles_usuario(cadena, departamento, usuarioroles)
        else:
            items = controllers.obtener_roles_usuario(usuarioroles)
    elif request.method == 'POST':
        if request.form["action"] == "Salir":
            session['busqueda'] = False
            return redirect(url_for('usuarios.administrar_usuarios'))
        elif request.form["action"] == "Buscar":
            cadena = request.form["cadena"]
            campo = request.form["campos"]
            session['cadena'] = cadena
            session['campos'] = campo
            items = controllers.buscar_roles_usuario(cadena, campo, usuarioroles)
            if len(items) == 0:
                session['busqueda'] = False
                flash("No se encontraron coincidencias")
            else:
                session['busqueda'] = True
        elif request.form["action"] == "Limpiar":
            session['busqueda'] = False
            pagina = 1
            items = controllers.obtener_roles_usuario(usuarioroles)
        elif request.form["action"] == "Asignar":
            return redirect(url_for('usuarios.asignar_roles_usuario', \
            usuarioroles=usuarioroles, pagina=1))
        elif request.form["action"] == "Quitar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_quitar_roles_usuario(seleccionados)
            if error == None: #Si paso las validaciones
                resultado = controllers.quitar_roles_usuario(seleccionados, \
                usuarioroles)
                if resultado == 'OK':
                    flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
                    items = controllers.obtener_roles_usuario(usuarioroles)
                else:
                    error = resultado

    usr = controllers.obtener_usuario(usuarioroles)

    if usr == None:
        error = "El usuario no existe"
        return render_template('user/administrar_roles_usr.html', \
        items=items, id_usuario=id_usuario, error=error, cadena=cadena, \
        set_etc=campo, pg_sgte=0, pg_ante=0)

    paginacion_siguiente = utils.paginacion_siguiente(items, \
    pagina, REGISTERS_PER_PAGE)
    paginacion_anterior = utils.paginacion_anterior(items, pagina)
    paginacion_primero = utils.paginacion_primero(items, pagina)
    paginacion_ultimo = utils.paginacion_ultimo(items, pagina, \
    REGISTERS_PER_PAGE)

    items = utils.paginacion(items, pagina, REGISTERS_PER_PAGE)

    return render_template('user/administrar_roles_usr.html', \
    items=items, id_usuario=id_usuario, error=error, cadena=cadena, \
    set_etc=campo, pg_ante=paginacion_anterior, pg_sgte=paginacion_siguiente, \
    pg_prim=paginacion_primero, pg_ultm=paginacion_ultimo)

@mod_user.route('/roles/asignar/<usuarioroles>/<int:pagina>', methods=['GET', 'POST'])
@login_required
@roles_accepted('ADMUSR')
def asignar_roles_usuario(usuarioroles, pagina):
    id_usuario = session['id_usuario']
    items = []
    error = None
    cadena = ""
    campo = "todos"
    if request.method == 'GET':
        busqueda = session['busqueda']
        if busqueda == True:
            cadena = session["cadena"]
            campo = session["campos"]
            items = controllers.buscar_roles_usuario_asignar(cadena, campo, usuarioroles)
        else:
            items = controllers.obtener_roles_usuario_asignar(usuarioroles)
    elif request.method == 'POST':
        if request.form["action"] == "Salir":
            session['busqueda'] = False
            return redirect(url_for('usuarios.roles_usuario', \
            usuarioroles=usuarioroles, pagina=1))
        elif request.form["action"] == "Buscar":
            cadena = request.form["cadena"]
            campo = request.form["campos"]
            session['cadena'] = cadena
            session['campos'] = campo
            items = controllers.buscar_roles_usuario_asignar(cadena, campo, usuarioroles)
            if len(items) == 0:
                session['busqueda'] = False
                flash("No se encontraron coincidencias")
            else:
                session['busqueda'] = True
        elif request.form["action"] == "Limpiar":
            session['busqueda'] = False
            pagina = 1
            items = controllers.obtener_roles_usuario_asignar(usuarioroles)
        elif request.form["action"] == "Asignar":
            seleccionados = request.form.getlist("seleccionados")
            error = controllers.validaciones_asignar_roles_usuario(seleccionados)
            if error == None: #Si paso las validaciones
                resultado = controllers.asignar_roles_usuario(seleccionados, \
                usuarioroles)
                if resultado == 'OK':
                    flash(u"La operaci\xf3n se realiz\xf3 con \xe9xito")
                    return redirect(url_for('usuarios.roles_usuario', \
                    usuarioroles=usuarioroles, pagina=1))
                else:
                    error = resultado

    usr = controllers.obtener_usuario(usuarioroles)

    if usr == None:
        error = "El usuario no existe"
        return render_template('user/asignar_roles_usr.html', \
        items=items, id_usuario=id_usuario, error=error, cadena=cadena, \
        set_etc=campo, pg_sgte=0, pg_ante=0)

    paginacion_siguiente = utils.paginacion_siguiente(items, \
    pagina, REGISTERS_PER_PAGE)
    paginacion_anterior = utils.paginacion_anterior(items, pagina)
    paginacion_primero = utils.paginacion_primero(items, pagina)
    paginacion_ultimo = utils.paginacion_ultimo(items, pagina, \
    REGISTERS_PER_PAGE)

    items = utils.paginacion(items, pagina, REGISTERS_PER_PAGE)

    return render_template('user/asignar_roles_usr.html', \
    items=items, id_usuario=id_usuario, error=error, cadena=cadena, \
    set_etc=campo, pg_ante=paginacion_anterior, pg_sgte=paginacion_siguiente, \
    pg_prim=paginacion_primero, pg_ultm=paginacion_ultimo)
