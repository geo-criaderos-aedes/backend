var map;

function init() {
    var geographic = new OpenLayers.Projection("EPSG:4326");
    var mercator = new OpenLayers.Projection("EPSG:900913");

    map = new OpenLayers.Map('map', {
        projection: mercator
    });

    var osm = new OpenLayers.Layer.OSM("Mapa Base");
    map.addLayer(osm);

    var layer_visitados = new OpenLayers.Layer.WMS("Puntos Visitados", "http://localhost:8090/geoserver/WS-dbservidor/wms", {
        layers: 'WS-dbservidor:puntos_visitados',
        transparent: true
    }, {
        isBaseLayer: false
    });
    map.addLayer(layer_visitados);

    var layer_febriles = new OpenLayers.Layer.WMS("Casos Febriles", "http://localhost:8090/geoserver/WS-dbservidor/wms", {
        layers: 'WS-dbservidor:puntos_febriles',
        fillColor: "blue",
        transparent: true
    }, {
        isBaseLayer: false
    });
    map.addLayer(layer_febriles);

    var layer_baldios = new OpenLayers.Layer.WMS("Terrenos Baldios", "http://localhost:8090/geoserver/WS-dbservidor/wms", {
        layers: 'WS-dbservidor:puntos_baldios',
        fillColor: "blue",
        transparent: true
    }, {
        isBaseLayer: false
    });
    map.addLayer(layer_baldios);

    var layer_gomerias = new OpenLayers.Layer.WMS("Gomerias", "http://localhost:8090/geoserver/WS-dbservidor/wms", {
        layers: 'WS-dbservidor:puntos_gomerias',
        fillColor: "blue",
        transparent: true
    }, {
        isBaseLayer: false
    });
    map.addLayer(layer_gomerias);

    var layer_criadero_a = new OpenLayers.Layer.WMS("Criaderos A", "http://localhost:8090/geoserver/WS-dbservidor/wms", {
        layers: 'WS-dbservidor:puntos_criadero_a',
        fillColor: "blue",
        transparent: true
    }, {
        isBaseLayer: false
    });
    map.addLayer(layer_criadero_a);

    var layer_criadero_b = new OpenLayers.Layer.WMS("Criaderos B", "http://localhost:8090/geoserver/WS-dbservidor/wms", {
        layers: 'WS-dbservidor:puntos_criadero_b',
        fillColor: "blue",
        transparent: true
    }, {
        isBaseLayer: false
    });
    map.addLayer(layer_criadero_b);

    var layer_criadero_c = new OpenLayers.Layer.WMS("Criaderos C", "http://localhost:8090/geoserver/WS-dbservidor/wms", {
        layers: 'WS-dbservidor:puntos_criadero_c',
        fillColor: "blue",
        transparent: true
    }, {
        isBaseLayer: false
    });
    map.addLayer(layer_criadero_c);

    map.addControl(new OpenLayers.Control.LayerSwitcher());

    map.setCenter(new OpenLayers.LonLat(-57.5985254, -25.284207).transform(geographic, mercator), 5);
}
