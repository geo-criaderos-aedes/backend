def paginacion(lista, pagina, elementos):
    resultado = []
    if lista != None:
        inicio = (pagina * elementos) - elementos
        fin = (inicio + elementos)

        if fin > len(lista):
            fin = len(lista)

        for indice in range(inicio, fin):
            resultado.append(lista[indice])

    return resultado

def paginacion_siguiente(lista, pagina, elementos):
    resultado = 0
    if lista != None:
        calc = pagina * elementos
        if calc < len(lista):
            resultado = (pagina + 1)

    return resultado

def paginacion_anterior(lista, pagina):
    resultado = 0
    if lista != None:
        resultado = (pagina -1)

    return resultado

def paginacion_primero(lista, pagina):
    resultado = 0
    if lista != None and pagina > 1:
        resultado = 1

    return resultado

def paginacion_ultimo(lista, pagina, elementos):
    resultado = 0
    if lista != None:
        resultado = len(lista) // elementos

        if len(lista) > resultado * elementos:
            resultado = resultado + 1

        if pagina >= resultado:
            resultado = 0

    return resultado

def nvl(parametro1, parametro2):
    if parametro1 != None:
        return parametro1
    else:
        return parametro2
